Pipe flow
=========

| ``Pipe_flow`` describes a typical flow of a incompressible fluid in a pipe.
| Simulating such a pipe, the Hagen-Poiseuille velocity profile is Expected
  as a result.

.. autoclass:: src.Pipe_flow
    :noindex:

Constructor
-----------

  .. hint::
    | The number of Cells for each direction should be between 
      :math:`(x_n-x_0) \leq n_i \leq (x_n-x_0)\cdot 50` where  
      :math:`x_n` is the end of the domain, :math:`x_0` the begin 
      of the domain and :math:`n_i` the number of cells, for each dimension.
    | **Note:** This might differ for some specific discretization like very 
      small or very large computational domains.
    | Furthermore it is suggested to use a non-quadratic grid with, e.g.
      :math:`n_x = 3\cdot n_y`.

.. automethod:: src.Pipe_flow.__init__

| If it is wished to simulate a pipe with an inflow velocity of 5 in x-direction 
  this can be done with:

.. code-block:: python

    >>> physical_problem = src.Pipe_flow(5)

System initialisation
---------------------

| The system is initialised with the inflow velocity in x-direction 
  and zero velocity in y-direction and pressure.

Boundary Conditions
-------------------

| For the velocity there are used Dirichlet boundary conditions at the 
  left side of the domain.
| Neumann boundary conditions are used at the right side.
  The domain is assumed to be large enough that the velocity is not changing
  at the right side of the domain.
| At the bottom and top edge of the pipe the no-slip condition is applied.
| For the pressure there are used Neumann boundary conditions at every side.

.. automethod:: src.Pipe_flow.apply_bcs_velocity
.. automethod:: src.Pipe_flow.apply_bcs_pressure

Expected Results
----------------

| For now we stick with the example from above and assume an inflow of 5[:math:`\frac{m}{s}`].
| In this case the Hagen-Poiseuille velocity profile is expected.
| Such a simulation can be run with:

.. code-block:: python

    >>> import cflowpy as cf
    >>> physical_problem_pipe = cf.Pipe_flow(5)
    >>> system = cf.System(0, 10, 40, 
                            0, 2, 20, 
                            0.1, 1.,
                            physical_problem_pipe)
    >>> u, v, p = system.solve(0.01, 5, 
                               time_update="Runge_Kutta_4", 
                               adapt_dt = 0.5)

| A domain of :math:`x \in [0,10], y \in [0,2]` with 25 cells in x-direction and 
  5 cells in y-direction is used.
| In this case we use a kinematic viscosity of :math:`0.1[\frac{mm^2}{s}]` 
  and a density of :math:`1[\frac{kg}{mm^3}]`
| The time step size is 0.01 seconds and 5 seconds are simulated.
| Furthermore, we used the Runge Kutta 4 algorithm to step forward in time.
| If the cfl-condition is not fullfilled we adapt the time step size to 
  set the cfl-number on 0.5.

The Result can be visualized with:

.. code-block:: python

    >>> fig = chorin.contourfigure(u, v, p, 
                                   contour=False, 
                                   velocity_profile = [5, 15, 25], 
                                   title="Velocity profile in a pipe")

.. image:: ../images/Pipe_flow.png
   :width: 600



Pressure driven pipe flow
=========================

| ``Pipe_flow_pressure_driven`` gives the opportunity to prescribe the 
  pressure on the boundaries with Dirichlet boundary conditions. 
| The pressure on the domain is linearly interpolated from this
  boundary values.
  
  .. warning::
    | In this case, it is highly recommended to set 
      ``n_iteration_pressure_poisson`` to zero, when using ``solve``
      from ``System``, since the pressure is prescribed and so we 
      do not need to solve the Poisson equation and can save a lot
      of computational effort.
  
  .. hint::
    | The suggested number of cells is the same as in :any:`pipe_flow`.	

Constructor
-----------

.. automethod:: src.Pipe_flow_pressure_driven.__init__

| If it is wished to simulate a pipe with no inflow and a prescribed pressure
  of 10:math:`\frac{N}{m^2}` on the left side and 0:math:`\frac{N}{m^2}` on 
  the right side, this can be done with:

.. code-block:: python

    >>> physical_problem = src.Pipe_flow_pressure_driven(0, 10, 0)

System initialisation
---------------------

| This system is initialised the same as ``Pipe_flow``, however the 
  pressure is initialised with the prescribed pressure gradient.

Boundary Conditions
-------------------

| The boundary conditions are the same as for ``Pipe_flow``, with one 
  difference.
| The pressure is described with Dirichlet boundary condition on
  the left and right side of the domain.
| The pressure in the domain itself is interpolated from this
  boundary values.

.. automethod:: src.Pipe_flow_pressure_driven.apply_bcs_velocity
.. automethod:: src.Pipe_flow_pressure_driven.apply_bcs_pressure