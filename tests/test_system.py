import cflowpy as cf
import numpy as np
import os


physical_problem = cf.Cavity_flow(1, "top")
system1 = cf.System(0, 10, 30, 0, 9, 29, 0.1, 1, physical_problem)
system2 = cf.System(0, 10, 30, 0, 9, 29, 0.1, 1, physical_problem)
u_1, v_1, p_1 = system1.solve(0.1,0.1)
u_2, v_2, p_2 = system1.solve(0.1,0.2, history = True)
integrator = cf.Integrator()

def test_System_initialization():
    system = cf.System(0, 10, 30, 0, 9, 29, 0.1, 1, physical_problem)
    grid_test = cf.Grid(0, 10, 30, 0, 9, 29)
    
    assert grid_same_test(system.grid, grid_test), 'Initialized grid is wrong'
    assert physical_problem_same_test(system.physical_problem, physical_problem), 'Physical Problem initialisation gone wrong'
    assert system.viscosity == 0.1, 'Initialized viscosity is wrong'
    assert system.density == 1, 'Initialized density is wrong'
    
def test_solve():
    """
    This is more or less a simple check if the return types are correct and
    if the function does what it should if history is set to true

    The "real" verification, whether the solve function does what it should 
    do is done via visualization, since the solution to problems like a 
    lid-driven cavity flow or a pipe flow are known and can be compared 
    to the solution from this algorithm via graphics.
    """
    assert np.allclose(u_1, u_2[1], rtol=1e-15, atol=1e-15), 'solve for u is wrong'
    assert np.allclose(v_1, v_2[1], rtol=1e-15, atol=1e-15), 'solve for v is wrong'
    assert np.allclose(v_1, v_2[1], rtol=1e-15, atol=1e-15), 'solve for p is wrong'
    assert isinstance(u_1, np.ndarray), 'dtype for u_1 is wrong'
    assert isinstance(v_1, np.ndarray), 'dtype for v_1 is wrong'
    assert isinstance(p_1, np.ndarray), 'dtype for p_1 is wrong'
    assert isinstance(u_2, list), 'dtype for u_2 is wrong'
    assert isinstance(v_2, list), 'dtype for v_2 is wrong'
    assert isinstance(p_2, list), 'dtype for p_2 is wrong'
    
def check_stability():
    assert system1.check_stability(u_1, v_1, 0.1),\
                                 'check_stability for case 1 is wrong'
    system1.dt = 1
    assert not system1.check_stability(u_1, v_1, 0.1),\
                                        'check_stability for case 2 is wrong'
                                        
def test_update_dt():
    cfl1 = [1.2, 0.8]
    cfl2 = [0.3, 1.3]
    u_dt, v_dt, p_dt = system2.solve(0.1,0.1, adapt_dt = 0.5)
    system2.update_dt(cfl1, u_dt, v_dt)
    assert np.allclose(system2.dt, 0.19999999999999998,
                       rtol=1e-10, atol=1e-10),\
                       'update_dt not working for u breaking cfl-condition'
    system2.dt = 1
    system2.update_dt(cfl2, u_dt, v_dt)
    assert np.allclose(system2.dt, 24.42612555713682,
                       rtol=1e-10, atol=1e-10),\
                       'update_dt not working for v breaking cfl-condition'

def test_update_time():
    system_Euler = cf.System(0, 10, 30, 0, 9, 29, 0.1, 1, physical_problem)
    system_Euler.dt = 0.1
    system_RK4 = cf.System(0, 10, 30, 0, 9, 29, 0.1, 1, physical_problem)
    system_RK4.dt = 0.1
    system_RK4.time_update = "Runge_Kutta_4"
    assert time_integration_same_test(u_1, u_2[-1], v_1, v_2[-1],
                                      integrator,
                                      integrator.explicit_Euler,
                                      system_Euler,
                                      0.001)
    assert time_integration_same_test(u_1, u_2[-1], v_1, v_2[-1],
                                      integrator,
                                      integrator.Runge_Kutta_4,
                                      system_RK4,
                                      0.001)
    
def test_check_residual():
    # checked values have a smallest norm difference of 0.0208
    assert (system1.check_residual(u_2, v_2, p_2, 0.36) == False),\
            'check residual not working'
    assert (system1.check_residual(u_2, v_2, p_2, 0.018) == True),\
            'check residual not working'
    
    
def test_io():
    u_1_save, v_1_save, p_1_save = system1.solve(0.01, 1, adapt_dt=0.5, 
                                                 adaptive_step_size_tolerance=1.,
                                                 history = True,
                                                 save_data="test")
    data = np.load("test.npz")
    assert np.allclose(data["x"], system1.grid.get_x()[0,:],
                       rtol=1e-15, atol=1e-15),\
                       'file io not working for x-values'
    assert np.allclose(data["y"], system1.grid.get_y()[:,0],
                       rtol=1e-15, atol=1e-15),\
                       'file io not working for y-values'
    assert(data["viscosity"] == system1.viscosity),\
                       'file io not working for viscosity'
    assert(data["density"] == system1.density),\
                       'file io not working for density'
    assert(data["integration_method"] == system1.time_update),\
                       'file io not working for time_update'
    assert(data["adapt_dt"] == system1.adapt_dt),\
                       'file io not working for cfl-condition constraint'
    assert np.allclose(data["end_time"], 1, rtol=1e-10, atol=1e-10),\
                       'file io not working for end time'
    assert(data["adaptive_step_size_tolerance"] ==\
           system1.adapt_step_tol),\
                       'file io not working for adaptive_step_size_tolerance'
    assert np.allclose(data["velocity_x_direction"], u_1_save,
                       rtol=1e-10, atol=1e-10),\
                       'file io not working for velocity in x-direction'
    assert np.allclose(data["velocity_y_direction"], v_1_save, 
                       rtol=1e-10, atol=1e-10),\
                       'file io not working for velocity in y-direction'
    assert np.allclose(data["pressure"], p_1_save, rtol=1e-10, atol=1e-10),\
                       'file io not working for pressure'
    data.close()
    os.remove("test.npz")
    
    
    
    
    
# Helper functions to check similarity
    
def grid_same_test(grid1:cf.Grid, grid2:cf.Grid):
    if not grid1.get_dx() == grid2.get_dx(): return False
    if not grid1.get_dy() == grid2.get_dy(): return False
    if not np.allclose(grid1.get_u(), grid2.get_u(), rtol=1e-15, atol=1e-15):\
                       return False
    if not np.allclose(grid1.get_v(), grid2.get_v(), rtol=1e-15, atol=1e-15):\
                       return False
    if not np.allclose(grid1.get_p(), grid2.get_p(), rtol=1e-15, atol=1e-15):\
                       return False
    if not np.allclose(grid1.get_x(), grid2.get_x(), rtol=1e-15, atol=1e-15):\
                       return False
    if not np.allclose(grid1.get_y(), grid2.get_y(), rtol=1e-15, atol=1e-15):\
                       return False
                   
    return True

def physical_problem_same_test(problem1:cf.PhysicalProblem,
                               problem2:cf.PhysicalProblem):
    u = np.ones((10,10), float)
    v = np.full_like(u, 1.4)
    p = np.full_like(u, 23.)
    if not (np.allclose(problem1.initialize_system(u, v, p),\
            problem2.initialize_system(u, v, p),\
            rtol=1e-15, atol=1e-15)):\
            return False
    if not (np.allclose(problem1.apply_bcs_velocity(u, v),\
                        problem2.apply_bcs_velocity(u, v),\
                        rtol=1e-15, atol=1e-15)):\
                        return False
    if not (np.allclose(problem1.apply_bcs_pressure(p),\
                        problem2.apply_bcs_pressure(p),\
                        rtol=1e-15, atol=1e-15)):\
                        return False
    
    return True

def time_integration_same_test(u1, u2, v1, v2,
                               integrator:cf.Integrator,
                               method:callable,
                               system:cf.System,
                               adaptive_step_size_tolerance:float):
    u_test_1 = method(u1, system.dt, u2)
    v_test_1 = method(v1, system.dt, v2)
    u_test_2, v_test_2 = system.update_time(u1, u2, v1, v2, second=False)
    
    if not np.allclose(u_test_1, u_test_2, rtol=1e-10, atol=1e-10):
                       return False
    if not np.allclose(v_test_1, v_test_2, rtol=1e-10, atol=1e-10):
                       return False
                   
    # adaptive step size tolerance first step
    u_adaptive_test_1, v_adaptive_test_1, dt_adaptive_test_1 =\
            integrator.adaptive_step_size(u1, v1,
                                          system.dt,
                                          u2, v2,
                                          adaptive_step_size_tolerance,
                                          method)
    system.adapt_step_tol = adaptive_step_size_tolerance
    u_adaptive_test_2, v_adaptive_test_2 = system.update_time(u1, u2,
                                                              v1, v2,
                                                              second=False)
    if not np.allclose(u_adaptive_test_1, u_adaptive_test_2,
                       rtol=1e-10, atol=1e-10):
                       return False
    if not np.allclose(v_adaptive_test_1, v_adaptive_test_2,
                       rtol=1e-10, atol=1e-10):
                       return False
    if not dt_adaptive_test_1 == system.dt_used: return False
    
    # adaptive step size tolerance second step
    u_second_adaptive_test_1 = method(u1, dt_adaptive_test_1, u2)
    v_second_adaptive_test_1 = method(v1, dt_adaptive_test_1, v2)
    u_second_adaptive_test_2, v_second_adaptive_test_2 =\
            system.update_time(u1, u2, v1, v2, second=True)
    if not np.allclose(u_second_adaptive_test_1, u_second_adaptive_test_2,
                       rtol=1e-10, atol=1e-10):
                       return False
    if not np.allclose(v_second_adaptive_test_1, v_second_adaptive_test_2,
                       rtol=1e-10, atol=1e-10):
                       return False
    
    return True
