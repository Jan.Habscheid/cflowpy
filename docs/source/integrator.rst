Integrator
==========

| ``Integrator`` is used to step forward in time with a numerical 
  integration method.
| In the purpose of this package a numerical integration is done to update
  the velocities for the next time step.

.. autoclass:: src.Integrator
    :noindex:

Constructor
-----------

The constructor needs no arguments.

.. automethod:: src.Integrator.__init__
    
Methods
-------

| There are a few different methods implemented to step forward in time.
| Each has it's own advantages and disadvantages.
| It is possible to get a list of all the different implemented integration methods with:

.. automethod:: src.Integrator.get_methods

Explicit Euler
--------------

| The ``explicit_Euler`` is a simple method to step forward in time.
| It is an explicit method with a simple forward discretization.
| Therefore, the accuracy of this method is not the best.
| However, it is a very fast method

.. automethod:: src.Integrator.explicit_Euler

Runge Kutta 4
-------------

| The fourth order Runge Kutta scheme is a more advanced method in comparison 
  to the explicit Euler.
| This method computes four different slopes to advance in time

.. automethod:: src.Integrator.Runge_Kutta_4

Adaptive step size method
-------------------------

| Additionally, it is possible to reach higher accuracy with an 
  adaptive step size method.

.. warning::
    The time step can get arbitrary small which can lead to 
    much longer compute time needed for the calculations.

.. automethod:: src.Integrator.adaptive_step_size
