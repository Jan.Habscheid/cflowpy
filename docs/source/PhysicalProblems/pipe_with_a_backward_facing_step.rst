Pipe with a backward facing step
================================


| In this example our physical domain is a pipe, but with a backward facing step 
  at the left side of the domain.
| Vortexes are expected to build behind the step.

.. autoclass:: src.Pipe_with_a_backward_facing_step
    :noindex:

Constructor
-----------

.. automethod:: src.Pipe_with_a_backward_facing_step.__init__

.. warning::

    | The arguments ``x_end_step`` and ``y_end_step`` define an **array index** 
      of the discretized domain, not the end of the physical domain.

  .. hint::
    | The number of Cells for each direction should be between 
      :math:`(x_n-x_0)\cdot 20 \leq n_i \leq (x_n-x_0)\cdot 50` where  
      :math:`x_n` is the end of the domain, :math:`x_0` the begin 
      of the domain and :math:`n_i` the number of cells, for each dimension.
    | **Note:** This might differ for some specific discretization like very 
      small or very large computational domains.
    | Furthermore it is suggested to use a non-quadratic grid with, e.g.
      :math:`n_x = 3\cdot n_y`.


| **A small example**: 
| The x-direction goes from 0 to 10 with 100 grid points and 
  ``x_end_step`` is initialised with 7.
| In this case the step won't stop at the x coordinate 7, 
  but at 0.7 since this is the seventh index of our discretized domain.    
| It is possible to construct such a physical problem with the following:

.. code-block:: python

    >>> physical_problem = cf.Pipe_with_a_backward_facing_step(7, 1, 7)

| In this case it is important how the physical domain is discretized.
| The physical domain has to have more points than the Indices for 
  ``x_step_end`` and ``y_step_end``.
| This is not checked in the instantiation of 
  ``Pipe_with_a_backward_facing_step`` since the definition of the physical
  problem is splitted from the definition of the grid itself.

System initialisation
---------------------

| The velocity in x-direction is initialized with ``u_left`` 
  everywhere, except in the step itself.
| The velocity in y-direction and the pressure are initialised to zero.

.. automethod:: src.Pipe_with_a_backward_facing_step.initialize_system

Boundary Conditions
-------------------

| As in :any:`pipe_flow`, Dirichlet boundary conditions are used at the
  left edge of the domain for the velocities.
| Neumann boundary conditions are used at the right side and the no-slip 
  condition is applied at the bottom and top edge.
| For the pressure there are used Neumann boundary conditions on each 
  side of the domain and the pressure inside the step 
  is set to zero in each iteration.

.. automethod:: src.Pipe_with_a_backward_facing_step.apply_bcs_velocity
.. automethod:: src.Pipe_with_a_backward_facing_step.apply_bcs_pressure

Expected Results
----------------

We expect two results occuring.
First, we would expect the **Hagen-Poiseuille velocity** profile before the step.
Additionally, at some distance behind the step we would expect a slightly shifted Hagen-Poiseuille velocity profile.
Furthermore, we expect **vortexes** immediately behind the step.
To get these results we can do the following:

.. code-block:: python

    >>> import cflowpy as cf
    >>> physical_problem_pipe_step = cf.Pipe_with_a_backward_facing_step(60, 20, 5)
    >>> system_pipe_step = cf.System(0, 15, 200,
                                    0, 5, 100,
                                    0.1, 1, 
                                    physical_problem_pipe_step)
    >>> u, v, p = system_pipe_step.solve(0.01, 1, 
                                         n_iteration_pressure_poisson = 50, 
                                         adapt_dt = 0.5)

| To ensure numerical stability we use ``adapt_dt`` which adapts the time
  step length if the cfl-condition is not fullfilled.
| To speed up the calculations we set the number of iteration in the 
  Poisson-Equation to 50 instead of the default value of 100.

| And again we want to visualize our results:

fig = system_pipe_step.contourfigure(u, v, p, 
                                     contour=False, velocity="streamline",
                                     velocity_profile = [20, 50, 100, 150],
                                     title="velocity profile of a pipe flow with a backward facing step")

.. image:: ../images/Pipe_step.png
   :width: 600