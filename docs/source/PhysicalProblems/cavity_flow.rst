Cavity flow
===========

| For the cavity flow a domain with three rigid walls is faced.
| On three of the four walls the no-slip condition is applied
  and on the fourth wall a lid is moving with a tangential unit 
  velocity.

.. autoclass:: src.Cavity_flow
    :noindex:

Constructor
-----------

.. automethod:: src.Cavity_flow.__init__

| Typically, in the case of a cavity flow, a quadratic domain would be used.
| By initializing the physical domain, this should be kept in mind.
| Large differences between the number of grid points or between the domain 
  sizes in x and y-direction can lead to inaccurate results.

  .. hint::
    | The number of Cells for each direction should be between 
      :math:`(x_n-x_0)\cdot 2 \leq n_i \leq (x_n-x_0)\cdot 50` where  
      :math:`x_n` is the end of the domain, :math:`x_0` the begin 
      of the domain and :math:`n_i` the number of cells, for each dimension.
    | **Note:** This might differ for some specific discretization like very 
      small or very large computational domains.

| A possible initialisation could be:

.. code-block:: python

    >>> physical_problem = src.Cavity_flow(1, "top")

| In this case the lid is moving with a velocity of 1 from the left to the
  right on top of the domain.

System initialisation
---------------------

| For the cavity flow the system is initialised with the boundary Conditions
  and has no special construction for the first time step.

Boundary Conditions
-------------------

| Dirichlet Boundary conditions, in form of the no-slip condition, are applied
  to three of the four walls for the velocity and Neumann boundary conditions
  for the pressure.
| On the fourth side of the domain the the lid is moving with a tangential
  unit velocity.
| At this side of the domain Dirichlet boundary conditions are applied for
  the velocity and for the pressure.

.. automethod:: src.Cavity_flow.apply_bcs_velocity
.. automethod:: src.Cavity_flow.apply_bcs_pressure

Expected Results
----------------

| The velocity profile across the vertical centerline of the cavity is 
  expected to be parabolic, for the case of the lid moving at the top 
  or bottom edge of the domain.
| If the lid is moving at the left or right side, the velocity profile
  is expected to be parabolic across the horizontal centerline.
| Furthermore, the flow is expected to be symmetric along the centerlines.
| The streamlines are expected to be clockwise, if the lid is moving on the
  top side of the domain to the right direction, 
  which is creating a clockwise velocity profile.
| If the lid is moving from right to left, the velocity profile is expected
  to be counter clockwise.
| Additionally, the pressure is expected to be higher on the side where the
  lid is moving towards and to be lower on the side where it comes from.
| All in all, the simulation, as already done in the :any:`Tutorial`, can be 
  run with:


.. code-block:: python

    >>> import cflowpy as cf
    >>> physical_problem_cavity = cf.Cavity_flow(1, "top")
    >>> system = cf.System(0, 2, 30, 
                            0, 2, 30, 
                            0.1, 1, 
                            physical_problem_cavity)
    >>> u, v, p = system.solve(0.01, 5)

and visualized with:

.. code-block:: python

    >>>  fig = system.contourfigure(u, v, p, 
                                    cmap="coolwarm", 
                                    title="Lid-driven cavity flow")

.. image:: ../images/Cavity_streamline.png
   :width: 600
