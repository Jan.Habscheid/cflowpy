import numpy as np


class Integrator:
    """
    Used to do numerical integration.
    In our purpose this is used to update the velocity in time
    with different methods, such as the explicit Euler or Runge-Kutta 4.

    Attributes
    ----------
    methods : list
        A list with all different integration methods implemented
        to step forward in time.
        Take care: adaptive_step_size is also a method of this class. 
        However, it is no "integration" method, since it uses another
        integration method and tries to get a better accuracy,
    """

    def __init__(self):
        """
        Constructor

        Creates a list of possible integration methods.
        """
        self.methods = ["explicit_Euler", "Runge_Kutta_4"]

    def get_methods(self):
        """
        Get the implemented time integration methods

        Returns
        -------
        list
            List of all implemented time integration methods
        """
        return self.methods

    def explicit_Euler(self, 
                       u_n: np.ndarray, 
                       dt: float, 
                       u_rhs: np.ndarray):
        """
        Applies explicit Euler

        This is an explicit integration method.
        The following discretisation is applied:
        df/dt ≈ (f(t+dt) - f(t)) / (dt) = rhs
        ↔ f(t+dt) = f(t) + dt * rhs

        Note: No type checking is implemented since this method is only called
        in the function "Chorin.update_time" where all necessary types 
        are checked.
        Thus, it is highly recommended to check all necessary conditions
        if this method is used out of this scope

        Parameters
        ----------
        u_n : np.ndarray
            Value (velocity) at current time
        dt : float
            Length of time step
        u_rhs : np.ndarray
            Rhs of the equation

        Raises
        ------
        TypeError
            If u_n or u_rhs is no numpy ndarray
        TypeError
            If the time step size is no number
        ValueError
            If the time step size in negative

        Returns
        -------
        np.ndarray
            Approximate value at new time (current time + time step)
        """
        if not isinstance(u_n, np.ndarray):
            raise TypeError("u_n has to be a numpy ndarray")
        if not isinstance(dt, (float, int)):
            raise TypeError("dt has to be a number")
        if dt <= 0:
            raise ValueError("dt has to be positive")
        if not isinstance(u_rhs, np.ndarray):
            raise TypeError("u_rhs has to be a numpy ndarray")

        u_next = u_n + dt * u_rhs

        return u_next

    def Runge_Kutta_4(self, 
                      u_n: np.ndarray, 
                      dt: float, 
                      u_rhs: np.ndarray):
        """
        Applies Runge-Kutta 4 Method

        This is an explicit integration method of fourth order.

            - k1: slope at the beginning of the interval, using y
            - k2: slope at midpoint of the interval, using y and k1
            - k3: slope at midpoint of the interval, using y and k2
            - k4: slpope at end of interval, using y and k3

        The approximated new value is:

            - u_{n+1} = u_n + dt/6 * (k1 + 2*k2 + 2*k3 + k4)

        Find further explanation at:
        <https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods>

        Parameters
        ----------
        u_n : np.ndarray
            Value (velocity) at current time
        dt : float
            Length of time step
        u_rhs : np.ndarray
            Right-hand-side of the equation

        Raises
        ------
        TypeError
            If u_n or u_rhs is no numpy ndarray
        TypeError
            If the time step size is no number
        ValueError
            If the time step size in negative

        Returns
        -------
        np.ndarray
            Approximate value at new time (current time + time step)
        """
        if not isinstance(u_n, np.ndarray):
            raise TypeError("u_n has to be a numpy ndarray")
        if not isinstance(dt, (float, int)):
            raise TypeError("dt has to be a number")
        if dt <= 0:
            raise ValueError("dt has to be positive")
        if not isinstance(u_rhs, np.ndarray):
            raise TypeError("u_rhs has to be a numpy ndarray")

        k1 = u_rhs
        u_inter_1 = u_n + dt / 2 * k1
        k2 = u_rhs - u_inter_1
        u_inter_2 = u_n + dt / 2 * k2
        k3 = u_rhs - u_inter_2
        u_end = u_n + dt * k3
        k4 = u_rhs - u_end

        u_next = u_n + dt / 6 * (k1 + 2*k2 + 2*k3 + k4)

        return u_next

    def adaptive_step_size(
        self,
        u_n: np.ndarray,
        v_n: np.ndarray,
        dt: float,
        u_rhs: np.ndarray,
        v_rhs: np.ndarray,
        tolerance: float,
        method: callable,
    ):
        """
        Method to update in time with a adaptive step size.

        Tries to get higher accuracy in the time update.
        Therefore the solution in the next time step is calculated.
        Then the time step is halved and the solution according
        to this time step is calculated.
        This is repeated till the difference in both solutions
        is smaller then a given tolerance

        Parameters
        ----------
        u_n : np.ndarray
            Velocity in x-direction at current time
        v_n : np.ndarray
            Velocity in y-direction at current time
        dt : float
            Length of time step
        u_rhs : np.ndarray
            Right-hand-side of the equation for the velocity in x-direction
        v_rhs : np.ndarray
            Right-hand-side of the equation for the velocity in y-direction
        tolerance : float
            A Prescribed tolerance of how accurate the scheme should be
        method : callable
            The time update method which is used

        Raises
        ------
        TypeError
            If u_n, v_n, u_rhs or v_rhs is no numpy ndarray
        TypeError
            If the time step size or the tolerance is no number
        ValueError
            If the time step size or the tolerance is negative
        TypeError
            If the integration method is no function

        Returns
        -------
        np.ndarray, np.ndarray, float
            Approximate value at new time and the used time step length
            to compute this value
        """
        if not isinstance(u_n, np.ndarray):
            raise TypeError("u_n has to be a numpy ndarray")
        if not isinstance(v_n, np.ndarray):
            raise TypeError("v_n has to be a numpy ndarray")
        if not isinstance(dt, (float, int)):
            raise TypeError("dt has to be a number")
        if dt <= 0:
            raise ValueError("dt has to be positive")
        if not isinstance(u_rhs, np.ndarray):
            raise TypeError("u_rhs has to be a numpy ndarray")
        if not isinstance(v_rhs, np.ndarray):
            raise TypeError("v_rhs has to be a numpy ndarray")
        if not isinstance(tolerance, (float, int)):
            raise TypeError("tolerance has to be a number")
        if tolerance <= 0:
            raise ValueError(
                "negative tolerance is not meaningful, \
                              tolerance has to be positive"
            )
        if not hasattr(method, "__call__"):
            raise TypeError(
                "method has to be a one of the implemented \
                             function: ",
                str(self.get_methods()),
            )

        error = tolerance + 1
        while error > tolerance:
            u_full = method(u_n, dt, u_rhs)
            v_full = method(v_n, dt, v_rhs)
            dt *= 1 / 2
            u_half = method(u_n, dt, u_rhs)
            v_half = method(v_n, dt, v_rhs)
            
            u_change = np.abs(np.linalg.norm(u_full) - np.linalg.norm(u_half))
            v_change = np.abs(np.linalg.norm(v_full) - np.linalg.norm(v_half))
            error = max(u_change, v_change)

        return u_full, v_full, dt * 2
