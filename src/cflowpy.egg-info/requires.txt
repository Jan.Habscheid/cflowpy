numpy>=1.25.0
matplotlib>=3.7.1
imageio>=2.31.1
pytest>=7.4.0
