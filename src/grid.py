import numpy as np


class Grid:
    """
    This Class is used to generate a grid in rectangular coordinates.

    This Grid can be used to solve the incompressible Navier-Stokes
    momentum equation with Chorin's projection method.

    Attributes
    ----------
    x : float
        x-coordinates of the grid
    y : float
        y-coordinates of the grid
    dx : float
        Size between two points in x-direction
    dy : float
        Size between two points in y-direction
    u : float
        Velocity in x-direction (instantiated with zero)
    v : float
        Velocity in y-direction (instantiated with zero)
    p : float
        Pressure (instantiated with zero)
    """

    def __init__(self,
                 xBegin: float,
                 xEnd: float,
                 nCellsX: int,
                 yBegin: float,
                 yEnd: float,
                 nCellsY: int):
        """
        Constructor

        Generates a grid in rectangular coordinates with a desired
        size in x- and y-direction.
        Instantiates velocities in x- and y-direction and a pressure 
        with zeros.
        Computes the difference between grid-cells in x- and y-direction

        Parameters
        ----------
        xBegin : float
            Left side of the domain
        xEnd : float
            Right side of the domain
        nCellsX : int
            Number of Cells in x-direction
        yBegin : float
            Lower side of the domain
        yEnd : float
            Upper side of the domain
        nCellsY : int
            Number of Cells in y-direction

        Raises
        ------
        ValueError
            If the end of the domain in x-direction is lower then the 
            begin of the domain in x-direction
        ValueError
            If the end of the domain in y-direction is lower then the
            begin of the domain in y-direction
        ValueError
            If the number of cells in x or y-direction is smaller as one.
        TypeError
            If the number of cells in x or y-direction is no integer
        """

        if xEnd <= xBegin:
            raise ValueError("End of domain in x-direction has to be"
                             "larger then begin of domain in x-direction")
        if yEnd <= yBegin:
            raise ValueError("End of domain in y-direction has to be"
                             "larger then begin of domain in y-direction")
        if nCellsX < 1 or nCellsY < 1:
            raise ValueError("Positive number of cells needed")
        if isinstance(nCellsX, int) == False or\
           isinstance(nCellsY, int) == False:
            raise TypeError("Number of cells has to be an integer")

        nPointsX = nCellsX + 1
        nPointsY = nCellsY + 1
        x_space = np.linspace(xBegin, xEnd, nPointsX)
        y_space = np.linspace(yBegin, yEnd, nPointsY)
        self.x, self.y = np.meshgrid(x_space, y_space)
        self.dx = (xEnd - xBegin) / nCellsX
        self.dy = (yEnd - yBegin) / nCellsY

        self.u = np.zeros((nPointsY, nPointsX))
        self.v = np.zeros((nPointsY, nPointsX))
        self.p = np.zeros((nPointsY, nPointsX))

    def get_x(self):
        """
        Get access to the x-coordinates of the grid

        Returns
        -------
        np.array
            x-coordinates of the grid
        """
        return self.x

    def get_y(self):
        """
        Get access to the y-coordinates of the grid

        Returns
        -------
        np.array
            y-coordinates of the grid
        """
        return self.y

    def get_dx(self):
        """
        Get access to the distance between
               grid points in x-direction

        Returns
        -------
        float
            distance between grid points in x-direction
        """
        return self.dx

    def get_dy(self):
        """
        Get access to the distance between
               grid points in y-direction

        Returns
        -------
        float
            distance between grid points in y-direction
        """
        return self.dy

    def get_u(self):
        """
        Get access to the velocity in x-direction

        Returns
        -------
        np.array
            velocity in x-direction
        """
        return self.u

    def get_v(self):
        """
        Get access to the velocity in y-direction

        Returns
        -------
        np.array
            velocity in y-direction
        """
        return self.v

    def get_p(self):
        """
        Get access to the pressure of the grid

        Returns
        -------
        np.array
            pressure
        """
        return self.p

    ############################################################
    ####### From here finite differences are implemented #######
    ############################################################

    def central_fd(self, 
                   val: np.ndarray, 
                   i_para_dim: int, 
                   order: int):
        """
        Computes central finite-differences.

        Used to compute central finite-differences of a 2d-Matrix.
        Zeros will be returned at the boundary.

        It is highly recommended to always use this function instead of
        "central_fd_deg1" or "central_fd_deg2" since in those two functions
        there is no checking of the input.

        Parameters
        ----------
        val : np.array
            Input array to compute the central finite-differences on
        i_para_dim : int
            parametric dimension in which to compute the finite differences
            (0 for x, 1 for y)
        order : int
            Order of the finite differences (only order 1 and 2 implemented)

        Raises
        ------
        i_para_dim has to be 0 (central finite-difference in x-direction)
            or 1 (central finite-difference in y-direction)
            Check that the parametric dimension in which to compute the
            finite differences is one that is implemented.
        order has to be 1
            Checks that the order for the finite differences is one
            that is implemented.

        Returns
        -------
        np.array
            Central finite-differences
        """

        if i_para_dim not in [0, 1]:
            raise ("i_para_dim has to be "
                   "0 (central finite-difference in x-direction) "
                   "or 1 (central finite-difference in y-direction)")
        if order not in [1, 2]:
            raise ("order has to be 1 or 2")

        if order == 1:
            return self.central_fd_deg1(val, i_para_dim)
        elif order == 2:
            return self.central_fd_deg2(val, i_para_dim)

    def central_fd_deg1(self, 
                        val: np.ndarray, 
                        i_para_dim: int):
        """
        Computes Central finite differences of order 1

        Parameters
        ----------
        val : np.ndarray
            Input array to compute the central finite-differences on
        i_para_dim : int
            parametric dimension in which to compute the finite differences 
            (0 for x, 1 for y)

        Returns
        -------
        np.array
            Central finite-differences
        """
        mat = np.zeros_like(val, dtype=float)
        if i_para_dim == 0:
            mat[:,1:-1] = (val[:,2:None] - val[:,:-2]) / (2. * self.get_dx())
            return mat
        elif i_para_dim == 1:
            mat[1:-1,:] = (val[2:None,:] - val[:-2,:]) / (2. * self.get_dy())
            return mat

    def central_fd_deg2(self, 
                        val: np.ndarray, 
                        i_para_dim: int):
        """
        Computes Central finite differences of order 2

        Parameters
        ----------
        val : np.ndarray
            Input array to compute the central finite-differences on
        i_para_dim : int
            parametric dimension in which to compute the finite differences
            (0 for x, 1 for y)

        Returns
        -------
        np.array
            Central finite-differences
        """
        mat = np.zeros_like(val, dtype=float)
        if i_para_dim == 0:
            mat[:,1:-1] = (val[:,2:None] - 2 * val[:,1:-1] + val[:,:-2])\
                         / self.get_dx() ** 2
            return mat
        elif i_para_dim == 1:
            mat[1:-1,:] = (val[2:None,:] - 2 * val[1:-1,:] + val[:-2,:])\
                        / self.get_dy() ** 2
            return mat

    def laplace(self, 
                val: np.ndarray):
        """
        Computes Laplace-Operator of a "val""

        Used to compute the Laplace-Operator of a Matrix.
        Zeros will be returned at the boundary.

        Parameters
        ----------
        val : np.array
            Input array to compute the laplace-operator on,
            using central finite-differences of order 2

        Returns
        -------
        np.array
            Laplace-operator
        """

        mat = np.zeros_like(val, dtype=float)
        mat[1:-1,1:-1] = (
            self.central_fd(val, 0, 2)[1:-1,1:-1]
            + self.central_fd(val, 1, 2)[1:-1,1:-1]
        )

        return mat

    def jacobi(self,
               val: np.ndarray,
               rhs: np.ndarray,
               n_iteration: int,
               apply_bcs=None,
               residual: float = None):
        """
        Solves the Poisson Equation

        ∇(∇val) = rhs -> Laplace-Operator einfügen.
        Laplace-Operator and rhs are discretised using 
        central-finite differences.
        This discretices equation is transformed to val[i][j].
        Then val[i][j] is updated n_itertaion times regarding 
        the derived formula. 
        In this context we want to have special boundary-values 
        after each iteration.
        This Jacobi-Iteration is used to solve a pressure-poisson 
        equation from the incompressible Navier-Stokes so we want 
        to update the boundaries of thepressure in a specific kind of way.
        After each iteration the pressure is updated from the user-written
        function apply_bcs.

        Parameters
        ----------
        val : np.array
            Value from which one takes the Laplace operator for the lhs.
        rhs : np.array
            Right-hand-side of the Poisson Equation
        n_iteration : int
            Number of Jacobi iterations applied.
        apply_bcs : _type_, optional
            Function to apply boundary-conditions,
            by default None (no update will be applied)
        residual : float, None optional
            Decides whether the calculations should be stopped
            if the difference in norm in two following time steps
            is smaller then the residual (float),
            by default None

        Returns
        -------
        np.array
            Solution of the Poisson Equation.
        """

        if apply_bcs == None:
            apply_bcs = lambda f: f
        for _ in range(n_iteration):
            val_new = np.zeros_like(val, dtype=float)
            val_new[1:-1,1:-1] = (
                (
                    (val[2:None,1:-1] + val[:-2,1:-1]) / (self.get_dx()** 2)
                    + (val[1:-1,2:None] + val[1:-1,:-2]) / (self.get_dy()** 2)
                    - rhs[1:-1,1:-1]
                )
                * 1
                / (2 / self.get_dx() ** 2 + 2 / self.get_dy() ** 2)
            )
            val_new = apply_bcs(val_new)

            curResidual = np.abs(np.linalg.norm(val_new)\
                                 - np.linalg.norm(val))
            if residual != None and curResidual < residual:
                return val_new
            val = val_new

        return val
