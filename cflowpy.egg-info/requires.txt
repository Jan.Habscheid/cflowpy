numpy>=1.20.0
matplotlib>=3.7.1
imageio>=2.31.1

[build]
build>=0.7.0

[dev]
cflowpy[tests]
cflowpy[docs]
cflowpy[build]

[docs]
sphinx==6.2.1
myst-parser>=2.0.0
sphinx_rtd_theme>=1.2.2

[notebook]
jupyter>=1.0.0

[tests]
pytest>=7.4.0
