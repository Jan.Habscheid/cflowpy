from setuptools import setup, find_packages

setup(
    name="cflowpy",
    version="0.1",
    description="A python package to solve the incompressible Navier-Stokes "
                 "Equation with Chorins projection method",
    url="https://git.rwth-aachen.de/Jan.Habscheid/cflowpy",
    author="Jan Habscheid",
    author_email="Jan.Habscheid@rwth-aachen.de",
    packages=["cflowpy"],
    package_dir={"cflowpy":"src"},
    package_data={"cflowpy": ["tests/*"]},
    python_requires=">=3.9",
    install_requires=[
        "numpy>=1.20.0",
        "matplotlib>=3.7.1",
        "imageio>=2.31.1",
    ],
    setup_requires=["pytest-runner"],
    tests_require=["pytest>=7.4.0"],
    test_suite="tests"
)
