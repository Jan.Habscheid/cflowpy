import numpy as np
import imageio
import os
import matplotlib.pyplot as plt
from .integrator import Integrator
from .grid import Grid
from .physical_problem import PhysicalProblem

class System:
    """
    This class is used to solve the incompressible Navier-Stokes equation in
    the momentum equation with Chorins projection method.

    ∂u/∂t + (u • ∇)u = -1/ρ ∇p + ν∇²u + g
    
    With the constraint ∇ • u = 0 for incompressibility.
    
    In Terms of variables we talk about
    u: flow velocity
    t: time
    ρ: density
    p: pressure
    ν: kinematic viscosity
    g: body accelerations
    
    In this case we use g=0.
    The velocity field (u) lives in 2 dimensions.
    The velocity in x-direction and the one in y-direction.
    In our case we use a common definition and work with u as the velocity 
    in x-direction and v as the velocity in y-direction.
    The density and the kinematic viscosity are needed to be defined
    by construction.
    
    Attributes
    ----------
    grid : Grid
        An Instance of the class Grid to define the 
        discretized physical domain
    physical_problem : PhysicalProblem
        Defines the physical state of the problem.
    viscosity: float
        The kinematic viscosity of the fluid
    density: float
        The density of the fluid
    integrator : Integrator
        Used for time integration
    dx : float
        Size between two points in x-direction of the physical domain
    dy : float
        size between two points in y-direction of the physical domain
    dt : float
        Initial length of the time step
    time_update : str
        Method to step forward in time
    adapt_dt : float
        Used to adapt the length of a time step if the 
        cfl condition is not fullfilled
    adaptive_step_size_tolerance : float, int
        Tolerance to adapt the time step to get a higher
        accuracy for the time integration
    dt_used : float
        Used length of time step if a adaptive step size method is chosen
        Step size is adapted in the intermediate time step and then the 
        adapted step size is used for the "final" update in time in each step
    """
    

    def __init__(self, 
                 xBegin:float, 
                 xEnd:float, 
                 nCellsX:int,
                 yBegin:float, 
                 yEnd:float, 
                 nCellsY:int,
                 viscosity:float, 
                 density:float,
                 physical_problem:type[PhysicalProblem]):
        """
        Constructor

        Generates an instance of the class Chorin.
        Assigns the grid, physical problem, viscosity and the density
        to the associated class Attributes.
        Explicit Euler is assigned as the "default" time integration.

        Parameters
        ----------
        xBegin : float
            Left side of the domain
        xEnd : float
            Right side of the domain
        nCellsX : int
            Number of Cells in x-direction
        yBegin : float
            Lower side of the domain
        yEnd : float
            Upper side of the domain
        nCellsY : int
            Number of Cells in y-direction
        viscosity : float
            The kinematic viscosity of the fluid
        density : float
            The density of the fluid
        physical_problem : PhysicalProblem
            Defines the physical state of the problem
            
        Raises
        ------
        ValueError
            If the end of the domain in x-direction is lower then 
            the begin of the domain in x-direction
        ValueError
            If the end of the domain in y-direction is lower then 
            the begin of the domain in y-direction
        ValueError
            If the number of cells in x or y-direction is no positive number,
            at least 1 cell is required
        TypeError
            If the number of cells in x or y-direction is no integer
        ValueError
            If Viscosity is negative
        ValueError
            If Density is lower or equal to zero
        TypeError
            If physical_problem is not of the type PhysicalProblem
        """
        if xEnd <= xBegin:
            raise ValueError("End of domain in x-direction has to be larger "
                             "then begin of domain in x-direction")
        if yEnd <= yBegin:
            raise ValueError("End of domain in y-direction has to be larger "
                             "then begin of domain in y-direction")
        if nCellsX<1 or nCellsY<1:
            raise ValueError("Positive number of cells needed")
        if isinstance(nCellsX, int) == False or\
           isinstance(nCellsY, int) == False:
            raise TypeError("Number of cells has to be an integer")
        if viscosity<0:
            raise ValueError("Viscosity has to be positive(typical) \
                             or zero(inviscid fluid)")
        if density<=0:
            raise ValueError("Density has to be larger then zero "
                             "Density = Mass/Volume")
        if isinstance(physical_problem, PhysicalProblem) == False:
            raise TypeError("physical_problem has to be of the "
                            "type PhysicalProblem")
        
        self.grid = Grid(xBegin, xEnd, nCellsX,
                         yBegin, yEnd, nCellsY)
        self.physical_problem = PhysicalProblem(physical_problem)
        self.viscosity = viscosity
        self.density = density
        self.integrator = Integrator()
        self.time_update = "explicit_Euler"
        self.adapt_step_tol = None




    def solve(self, 
              dt:float, 
              t_end:int, 
              time_update:str="explicit_Euler", 
              residual:float=None,
              n_iteration_pressure_poisson:float=100,
              residual_pressure_poisson:float=None,
              history:bool=False,
              adapt_dt:float=None,
              adaptive_step_size_tolerance:float=None,
              save_data:str=None):
        """
        Calculates the solution for the incompressible Navier-Stokes Equation
        in the momentum equation with Chorin's Projection method.

        This Algorithm is a splitted Algorithm.
        First, only the viscous forces are taken into account and an 
        intermediate velocity is calculated.
        Second, the pressure forces are taken into account.
        Therefore a Poisson equation has to be solved and then the
        velocity is updated in time.
        This is done till the final time is reached or if a residual is give
        till the differnce between two time steps is very small.
        
        In each time-step it is checked if the cfl-condition is fullfilled.

        Parameters
        ----------
        dt : float
            Length of time step
        t_end : int
            End time
        time_update : str, optional
            Method to step forward in time
            Has to be one of the methods that is implemented in Integrator
            Default is "explicitEuler"
        residual : float optional
            Decides whether the calculations should be stopped
            if the difference in the norm of two following time steps
            is lower than the residual (float),
            by default None
        n_iteration_pressure_poisson : float optional
            Number of maximum iteration to solve the pressure poisson equation
        residual_pressure_poisson : float, None optional
            Decides wheter the calculations to solve the pressure poisson
            equation should be stopped at a small change in two consecutive
            iterations or run "n_iteration_pressure_poisson" steps
        history : bool, optional
            If False: Return values are the ones in the last time step
            stored in numpy.ndarrays of dimension (nPointsY, nPointsX)
            If True: Return values are those in every time step stored in a
            list each time step is a numpy.ndarray of dimension 
            (nPointsY, nPointsX),
            by default False
        adapt_dt : float, optional
            The cfl-number is checked in every time step.
            If adapt_dt is not specified and the cfl-condition is not 
            fullfilled the iteration will stop and return the last values.
            If adapt_dt is defined, dt will be adapted in a way that the
            cfl-number is set to this value.
            Choose a value between 0 and 1 for adapt_dt.
            For good numerical results a value between 0.5 and 0.8
            is suggested.
            A value close to 1 is not suggested since this leads to a 
            edge stable case, which can lead to errors, non physical results
            or results going to infinity. Be careful with this since this can
            lead to very small time steps which can cause a very long
            compute time.
            By default None
        adaptive_step_size_tolerance : float, optional
            If adaptive_step_size_dt is specified the time integration will
            use a method to adapt the step size in each time update to gain
            a higher accuracy according to the adaptive_step_size_tolerance,
            by default None

        Raises
        ------
        ValueError
            If dt is smaller or equal to zero
        TypeError
            If the end time is no integer or float
        ValueError
            If the end time is lower or equal to zero
        ValueError
            If the given time update method is not implemented
        ValueError
            If the residual is given but lower or equal to zero
            Then the same is achieved as to give no residual
        TypeError
            If "history" is no boolean
        TypeError
            If adapt_dt is specified, but no float
        ValueError
            If adapt_dt is specified, but not between 0 and 1
        TypeError
            If adaptive_step_size_tolerance is specified but no number
        ValueError
            If adaptive_step_size_tolerance is specified 
            but lower or equal to zero

        Returns
        -------
        np.ndarray, np.ndarray, np.ndarray | 
        list[np.ndarray], list[np.ndarray], list[np.ndarray]
            Returns velocity in x- and y-direction as well as the
            pressure-field.
        """
        if dt<=0:
            raise ValueError("Length of time step has to be positive.")
        if isinstance(t_end, (float, int)) == False:
            raise TypeError("End time (t_end) has to be a number.")
        if t_end <= 0:
            raise ValueError("End time has to be positive")
        if time_update not in self.integrator.get_methods():
            raise ValueError("time_update has to be one that is implemented"
                             "in Integrator: \n" \
                            + str(self.integrator.get_methods()))
        if residual != None and residual <= 0:
            raise ValueError("Residual has to be positive, otherwise it is"
                             "does the same as giving no residual")
        if isinstance(history, bool) == False:
            raise TypeError("History has to be a boolean.\n"
                            "True to get the history of the time-steps as "
                            "a return value, false if not.\n"
                            "By default False")
        if adapt_dt != None and isinstance(adapt_dt, float) == False:
            raise TypeError("adapt_dt has to be a float")
        if adapt_dt != None and (0<adapt_dt<1) == False:
            raise ValueError("adapt_dt has to be between zero and 1")
        if adaptive_step_size_tolerance != None \
           and not isinstance(adaptive_step_size_tolerance, (float, int)):
            raise TypeError("adaptive_step_size_tolerance has to be a number")
        if adaptive_step_size_tolerance != None \
            and adaptive_step_size_tolerance <= 0:
            raise ValueError("negative tolerance for the adaptive step size\
                              is not meaningful, \
                              tolerance has to be positive")  
        
        grid = self.grid
        density = self.density
        viscosity = self.viscosity
        physical_problem = self.physical_problem

        # Get starting values
        u = grid.get_u()
        v = grid.get_v()
        p = grid.get_p()
        u, v, p = physical_problem.initialize_system(u, v, p)
        u = [u]
        v = [v]
        p = [p]
        
        # Get discretization
        self.dx = grid.get_dx()
        self.dy = grid.get_dy()
        dt_start = dt
        self.dt = dt
        self.time = []
        
        # Get time update properties
        self.time_update = time_update
        self.adapt_dt = adapt_dt
        self.adapt_step_tol = adaptive_step_size_tolerance

        cur_time = 0
        # Repeat till the desired end time is reached or
        while cur_time < t_end:
            # check for cfl-condition
            if not self.check_stability(u[-1], v[-1], cur_time):
                break
                        
            # Get derivatives of old velocities
            u_diff_x = grid.central_fd(u[-1], 0, 1)
            u_diff_y = grid.central_fd(u[-1], 1, 1)
            v_diff_x = grid.central_fd(v[-1], 0, 1)
            v_diff_y = grid.central_fd(v[-1], 1, 1)
            u_laplace = grid.laplace(u[-1])
            v_laplace = grid.laplace(v[-1])
            
            # Compute intermediate velocities
            u_rhs = -(u[-1] * u_diff_x + v[-1] * u_diff_y) \
                  + viscosity * u_laplace
            v_rhs = -(u[-1] * v_diff_x + v[-1] * v_diff_y) \
                  + viscosity * v_laplace
            u_intermediate, v_intermediate = \
                self.update_time(u[-1], u_rhs, v[-1], v_rhs)
            u_intermediate, v_intermediate = \
                physical_problem.apply_bcs_velocity(u_intermediate, 
                                                    v_intermediate)

            # Get derivatives of intermediate velocities
            u_intermediate_diff_x = \
                  grid.central_fd(u_intermediate, 0, order=1)
            v_intermediate_diff_y = \
                  grid.central_fd(v_intermediate, 1, order=1)
            
            # solve pressure equation
            rhs_poisson = density/dt * \
                        (u_intermediate_diff_x + v_intermediate_diff_y)
            p_new = \
                grid.jacobi(p[-1], rhs_poisson,
                            n_iteration_pressure_poisson,
                            physical_problem.apply_bcs_pressure,
                            residual_pressure_poisson)
            
            # Get pressure derivatives
            p_diff_x = grid.central_fd(p_new, 0, 1)
            p_diff_y = grid.central_fd(p_new, 1, 1)
            u_new, v_new = self.update_time(u_intermediate, -p_diff_x/density,\
                                            v_intermediate, -p_diff_y/density,\
                                            second=True)
            
            # Compute new time step
            u_new, v_new = \
                physical_problem.apply_bcs_velocity(u_new, v_new)   

            # update history arrays
            u.append(u_new)
            v.append(v_new)
            p.append(p_new)
            
            # Update time
            # Stepped two times forward in time with dt_used, once for the
            # intermediate velocity and once for the final new velocity
            cur_time += 2*self.dt_used
            self.time.append(cur_time)
            
            # check for convergence
            if self.check_residual(u, v, p, residual) == False:
                print("Convergence reached after " + str(cur_time) + 
                      "seconds, simulation stopped")
                break
        
        if save_data != None:
            self.save_data(save_data, u, v, p, dt_start, cur_time, residual,
                           n_iteration_pressure_poisson,
                           residual_pressure_poisson, self.adapt_dt)
            
            
        if history == True:
            return u, v, p
    
        return u[-1], v[-1], p[-1]
    
    
    
    def check_stability(self, 
                        u:np.ndarray, 
                        v:np.ndarray, 
                        time:float):
        """
        Checks for stability of numerical method.

        Checks the cfl-condition.
        We want to fullfill the condition that the absolute value
        of the cfl-number is not lower 0 or higher then 1.

        Parameters
        ----------
        u : np.ndarray
            Velocity in x-direction
        v : np.ndarray
            Velocity in y-direction
        time : float
            Current time

        Returns
        -------
        bool
            Returns True if the stability constraint is fullfilled, else False
        """
        
        cfl = np.array([np.max(np.abs(u*self.dt/self.dx)),\
                        np.max(np.abs(v*self.dt/self.dy))])
        stability = True 
        if not np.max(cfl) < 1 :
            stability = False
            if self.adapt_dt != None:
                self.update_dt(cfl, u, v)
                stability = True
            else:
                print("cfl number not between 0 and 1, "
                      "no stability guaranteed\n"
                     "iteration stopped at " + str(time) + " seconds. \n"
                     "Maximum absolute value for the cfl-number in"
                     "x-direction: ", cfl[0], " \n"
                     "Maximum absolute value for the cfl-number in"
                     "y-direction: ", cfl[1])
                
                
        return stability
    
    
    
    def update_dt(self, 
                  cfl:np.ndarray,
                  u:np.ndarray, 
                  v:np.ndarray):
        """
        Updates the time step size.

        The cfl-condition should be fullfilled for this simulation.
        If the cfl-condition is not fullfilled one can use this function
        to update the time step length, such that the cfl-condition
        is fullfilled again.
        There is no need to check that the maximum of the absolute value
        of u or v is zero since the cfl-condition would be satisfied
        for this case.

        Parameters
        ----------
        cfl : np.ndarray
            Maximum cfl-number in x and y-direction
        u : np.ndarray
            Velocity in x-direction
        v : np.ndarray
            Velocity in y-direction
        """
        
        # dt adapted in order of x-direction
        if cfl[0] > cfl[1]:
            self.dt = (cfl[0]*self.adapt_dt*self.dx)/np.max(np.abs(u))
        # cfl adapted in order of y-direction
        else:
            self.dt = (cfl[1]*self.adapt_dt*self.dy)/np.max(np.abs(v))
    
    
    def update_time(self, 
                    u:np.ndarray,
                    u_rhs:np.ndarray, 
                    v:np.ndarray, 
                    v_rhs:np.ndarray, 
                    second=False):
        """
        Updates the velocity in time with the given method
        
        Note: No type checking is implemented since this method is only called
        in the function "system" where all necessary types are checked.
        Thus, it is highly recommended to check all necessary conditions
        if this method is used out of this scope

        Parameters
        ----------
        u : np.ndarray
            Velocity in x-direction
        u_rhs : np.ndarray
            RHS of the equation to update velocity in x-direction
        v : np.ndarray
            Velocity in y-direction
        v_rhs : np.ndarray
            RHS of the equation to update velocity in y-direction
        dt : float
            Length of time step
        method : function
            Method to update the velocities in time

        Returns
        -------
        np.ndarray, np.ndarray
            Updated velocities in time
        """
        explicit_Euler = self.integrator.explicit_Euler
        Runge_Kutta_4 = self.integrator.Runge_Kutta_4
        # no adaptive step size
        if self.time_update == "explicit_Euler" and \
           self.adapt_step_tol == None:
            u = explicit_Euler(u, self.dt, u_rhs)
            v = explicit_Euler(v, self.dt, v_rhs)
            self.dt_used = self.dt
        elif self.time_update == "Runge_Kutta_4" and \
           self.adapt_step_tol == None:
            u = Runge_Kutta_4(u, self.dt, u_rhs)
            v = Runge_Kutta_4(v, self.dt, v_rhs)
            self.dt_used = self.dt
        
        # adaptive step size
        if self.time_update == "explicit_Euler" and \
           self.adapt_step_tol != None:
            if second==True:
                u = explicit_Euler(u, self.dt_used, u_rhs)
                v = explicit_Euler(v, self.dt_used, v_rhs)
            else:
                u, v, dt_used = \
                    self.integrator.adaptive_step_size(u, v,\
                                                       self.dt,\
                                                       u_rhs, v_rhs,\
                                                       self.adapt_step_tol,\
                                                       explicit_Euler)
                self.dt_used = dt_used
        if self.time_update == "Runge_Kutta_4" and \
           self.adapt_step_tol != None:
            if second==True:
                u = Runge_Kutta_4(u, self.dt_used, u_rhs)
                v = Runge_Kutta_4(v, self.dt_used, v_rhs)
            else:
                u, v, dt_used = \
                    self.integrator.adaptive_step_size(u, v,\
                                                       self.dt,\
                                                       u_rhs, v_rhs,\
                                                       self.adapt_step_tol,\
                                                       Runge_Kutta_4)
                self.dt_used = dt_used
                
                
        return u, v
    
    
    def check_residual(self, 
                       u:np.ndarray, 
                       v:np.ndarray, 
                       p:np.ndarray, 
                       residual:float=None):
        """
        Checks if the simulation should be stopped according to the residual.

        If the norm in the current time step subtracted to the norm of the
        last time step is smaller then the residual the simulation stops
        This is checked for all, the velocity in x- and y-direction 
        as well as the pressure field.

        Parameters
        ----------
        u : np.ndarray
            Velocity in x-direction
        v : np.ndarray
            Velocity in y-direction
        p : np.ndarray
            Pressure field
        residual : float, optional
            The residual to stop the iteration, by default None

        Returns
        -------
        bool
            False if the iteration should be stopped according to the residual
            If no residual is specified True is returned
        """
        change_u = np.abs(np.linalg.norm(u[-1])-np.linalg.norm(u[-2]))
        change_v = np.abs(np.linalg.norm(v[-1])-np.linalg.norm(v[-2]))
        change_p = np.abs(np.linalg.norm(p[-1])-np.linalg.norm(p[-2]))
        if residual != None and\
           all(np.array([change_u, change_v, change_p]) < residual):
            return False
        
        return True
    
    
    
    def save_data(self, 
                  file:str,
                  u:np.ndarray, 
                  v:np.ndarray, 
                  p:np.ndarray,
                  dt:np.ndarray,
                  t_end:float,
                  residual:float,
                  n_iteration_pressure_poisson:int,
                  residual_pressure_poisson:float,
                  adapt_dt:float):
        """
        Saves the simulation results with the simulation parameter.

        The results and parameters are saved in a single 
        uncompressed .npz file.

        Parameters
        ----------
        file : str
            File to save the results and parameter
        u : np.ndarray
            Velocity in x-direction
        v : np.ndarray
            Velocity in y-direction
        p : np.ndarray
            Pressure field
        dt : float
            Length of time step at beginning of simulation
        t_end : float
            End time of the simulation
        residual : float
            Residual, if used to stop at convergence
        n_iteration_pressure_poisson : int
            Number of iteration for the jacobi method to solve
            the poisson equation
        residual_pressure_poisson : float
            Residual for the poisson equation to stop at convergence
        adapt_dt : float
            cfl-condition constraint
        """
        np.savez(file,
                 x = self.grid.get_x()[0,:],
                 y = self.grid.get_y()[:,0],
                 viscosity = self.viscosity,
                 density = self.density,
                 dt = dt,
                 integration_method = self.time_update,
                 adapt_dt = adapt_dt,
                 end_time = t_end,
                 adaptive_step_size_tolerance = \
                        self.adapt_step_tol,
                 residual = residual,
                 n_iteration_pressure_poisson = n_iteration_pressure_poisson,
                 residual_pressure_poisson = residual_pressure_poisson,
                 velocity_x_direction = u,
                 velocity_y_direction = v,
                 pressure = p)
        
        
            
    def contourfigure(self, 
                      u:np.ndarray,
                      v:np.ndarray, 
                      p:np.ndarray, 
                      contour:bool=True,
                      title:str=None, 
                      xlabel:str="X", 
                      ylabel:str="Y", 
                      figsize:tuple=None,
                      velocity:str="quiver", 
                      alpha:float=None, 
                      velocity_color:str="black", 
                      velocity_profile:list=None,
                      cmap:str="viridis",
                      colorbar:bool=True,
                      savefig:str=None):
        """
        Generates a figure to visualize the simulation results.

        Can visualize the pressure as a contourfigure as well as the
        the velocity field and the streamlines.

        Parameters
        ----------
        u : np.ndarray
            velocity in x-direction
        v : np.ndarray
            velocity in y-direction
        p : np.ndarray
            pressure field
        contour : bool, optional
            If true, visualizes the pressure as a contourfigure, 
            by default True
        title : str, optional
            Title of the figure, by default None
        xlabel : str, optional
            Label of the x-axis, by default "X"
        ylabel : str, optional
            Label of the y-axis, by default "Y"
        figsize : tuple, optional
            Size of the figure, by default (6,6)
        velocity : str, optional
            method to visualize the velocity, choose "quiver" to get the
            velocity field and "streamline" to get the streamlines visualized,
            by default "quiver"
        alpha : float, optional
            alpha value for the quivers in matplotlib.pyplot.quiver, 
            by default None
        velocity_color : str, optional
            Color of the visualized velocities, by default "black"
        velocity_profile : list, optional
            Visualizes the velocity profile at the x-points specified
            in the argument, by default None
        cmap : str, optional
            Colormap used for the contourfigure, by default "viridis"
        colorbar : bool, optional
            Adds a colorbar for the pressure, by default True
        savefig : str, optional
            specifies in which file to save the visualization, by default None

        Returns
        -------
        matplotlib.pyplto.figure
            Visualized results

        Raises
        ------
        TypeError
            u, v or p is no np.ndarray
        TypeError
            contour is no boolean
        TypeError
            title is specified but no string
        TypeError
            x- or ylabel is no string
        TypeError
            figsize is no tuple
        ValueError
            velocity is neither quiver nor streamline
        TypeError
            alpha is no float
        ValueError
            alpha is not in the range of (0,1)
        TypeError
            velocity_color is no string
        TypeError
            velocity_profile is not list
        TypeError
            cmap is no string
        TypeError
            colorbar is no boolean
        TypeError
            savefig is specified but no string
        """
        if not isinstance(u, np.ndarray):
            raise TypeError("u has to be an n.ndarray")
        if not isinstance(v, np.ndarray):
            raise TypeError("v has to be a np.ndarray")
        if not isinstance(p, np.ndarray):
            raise TypeError("p has to be a np.ndarray")
        if not isinstance(contour, bool):
            raise TypeError("contour has to be a boolean")
        if title!= None and not isinstance(title, str):
            raise TypeError("title has to be a string, if specified")
        if not isinstance(xlabel, str):
            raise TypeError("xlabel has to be a string")
        if not isinstance(ylabel, str):
            raise TypeError("ylabel has to be a string")
        if figsize!=None and not isinstance(figsize, tuple):
            raise TypeError("figsize has to be a tuple, if specified")
        if velocity not in ["quiver", "streamline"]:
            raise ValueError("velocity has to be one of the following:",
                             "quiver, streamline")
        if alpha != None and not isinstance(alpha, float):
            raise TypeError("alpha has to be a float")
        if alpha != None and alpha < 0 and alpha > 1:
            raise ValueError("alpha has to be in range (0,1)")
        if not isinstance(velocity_color, str):
            raise TypeError("velocity_color has to be a string")
        if velocity_profile!=None and not isinstance(velocity_profile, list):
            raise TypeError("velocity_profile has to be a list")
        if not isinstance(cmap, str):
            raise TypeError("cmap has to be a string, "
                            "only matplotlib colormaps available")
        if not isinstance(colorbar, bool):
            raise TypeError("colorbar has to be a boolean")
        if savefig!=None and not isinstance(savefig, str):
            raise TypeError("savefig has to be a string with the desired"
                            "place to save the figure")

        if figsize==None:
            fig, ax = plt.subplots()
        if figsize!=None:
            fig, ax = plt.subplots(figsize=figsize)
        if contour == False:
                colorbar = False
        if contour == True:
            contourf_ = ax.contourf(self.grid.get_x(), self.grid.get_y(), 
                                    p, cmap=cmap)
        if colorbar:
            fig.colorbar(contourf_, label="Pressure")
        if velocity=="quiver":
            ax.quiver(self.grid.get_x(), self.grid.get_y(), 
                      u, v, alpha=alpha, 
                      color=velocity_color)
        elif velocity=="streamline":
            ax.streamplot(self.grid.get_x(), self.grid.get_y(), 
                          u, v,
                          color=velocity_color)
        if velocity_profile != None:
            for i in velocity_profile:
                plt.plot(self.grid.get_x()[:,i] + u[:,i],
                         self.grid.get_y()[:,i], 
                         color="red")
            
        if title!=None:
            ax.set_title(title)
        if xlabel!=None:
            ax.set_xlabel(xlabel)
        if ylabel!=None:
            ax.set_ylabel(ylabel)
        if savefig!=None:
            fig.savefig(savefig)
            
            
        return fig
    
    
    
    def create_gif(self, 
                   savefig:str,
                   u:list[np.ndarray],
                   v:list[np.ndarray],
                   p:list[np.ndarray],
                   contour:bool=True,
                   title:str=None, 
                   xlabel:str="X", 
                   ylabel:str="Y",
                   figsize:tuple=None,
                   velocity:str="quiver", 
                   alpha:float=None, 
                   velocity_color:str="black", 
                   velocity_profile:list=None,
                   cmap:str="viridis",
                   colorbar:bool=True,
                   skip_time:float=1,
                   duration:int=5,
                   time_digits:int=4,
                   loop:int=0):
        """
        Generates an animated gif to visualize the simulation results.

        Can visualize the pressure as a contourfigure as well as the
        the velocity field and the streamlines.
        Note, the typecheks that are done in ``contourfigure`` are not 
        repeated here, since this function calls ``contourfigure`` and
        so these values are checked.
        Saves the animated gif in the file specified in savefig.

        Parameters
        ----------
        u : list[np.ndarray]
            velocity in x-direction over time
        v : list[np.ndarray]
            velocity in y-direction over time
        p : list[np.ndarray]
            pressure field over time
        savefig : str
            specifies in which file to save the gif, by default None
        contour : bool, optional
            If true, visualizes the pressure as a contourfigure, 
            by default True
        title : str, optional
           Title of the figure, by default None, by default None
        xlabel : str, optional
            Label of the x-axis, by default "X"
        ylabel : str, optional
            Label of the y-axis, by default "Y"
        figsize : tuple, optional
            Size of the figure, by default None
        velocity : str, optional
           method to visualize the velocity, choose "quiver" to get the
            velocity field and "streamline" to get the streamlines visualized,
            by default "quiver"
        alpha : float, optional
            alpha value for the quivers in matplotlib.pyplot.quiver, 
            by default None
        velocity_color : str, optional
            Color of the visualized velocities, by default "black"
        velocity_profile : list, optional
            Visualizes the velocity profile at the x-points specified
            in the argument, by default None
        cmap : str, optional
            Colormap used for the contourfigure, by default "viridis"
        colorbar : bool, optional
            Adds a colorbar for the pressure, by default True
        skip_time : float, optional
            Skips time steps to visualize not every singel time step, by default 1
        duration : int, optional
            duration each frame lasts[ms], by default 5
        time_digits : int, optional
            digits of the time[s], by default 4
        loop : int, optional
            how often the gif repeats, 0 for endless repetition, by default 0

        Raises
        ------
        TypeError
            u, v or p is no list
        TypeError
            figsize is specified but no tuple
        TypeError
            skip_time is no number
        TypeError
            duration is no integer
        TypeError
            time_digits is no integer
        TypeError
            loop is no integer
        """
        if not isinstance(u, list):
            raise TypeError("u has to be a list")
        if not isinstance(v, list):
            raise TypeError("v has to be a list")
        if not isinstance(p, list):
            raise TypeError("p has to be a list")
        if figsize!=None and not isinstance(figsize, tuple):
            raise TypeError("figsize has to be a tuple, if specified")
        if not isinstance(skip_time, (float, int)):
            raise TypeError("skip_time has to be a number "
                            "(frames to skip in each iteration)")
        if not isinstance(duration, int):
            raise TypeError("duration has to be a integer "
                            "(ms to last for each frame)")
        if not isinstance(time_digits, int):
            raise TypeError("time_digits has to be an integer")
        if not isinstance(loop, int):
            raise TypeError("loop has to be an integer")
        
        
        def create_frame(i):
            fig = plt.figure()
            if figsize!=None:
                fig = plt.figure(figsize=figsize)    
            
            if title != None:
                title_gif = title + "\n time = " + str(round(float(self.time[i]), time_digits)) + "s"
            else:
                title_gif = "time = " + str(round(float(self.time[i]), time_digits)) + "s"
            fig = self.contourfigure(u[i], v[i], p[i], contour, title=title_gif,
                                     xlabel=xlabel, ylabel=ylabel, 
                                     figsize=figsize,
                                     velocity=velocity,
                                     alpha=alpha, velocity_color=velocity_color, 
                                     velocity_profile=velocity_profile,
                                     cmap=cmap, colorbar=colorbar)
            fig.savefig(f'img_{t}.png',
                        transparent=False,
                        facecolor='white'
                        )
            plt.close('all')
            
        frames = []
        for t in range(0, len(self.time), skip_time):
            create_frame(t)
            image = imageio.v2.imread(f'img_{t}.png')
            os.remove(f'img_{t}.png')
            frames.append(image)
            
        imageio.mimsave(savefig,
                        frames,
                        duration = duration,
                        loop = loop)
        