Installation
============

Recommended installation with pip and git+
------------------------------------------

| Run 

```console
pip install git+https://git.rwth-aachen.de/Jan.Habscheid/cflowpy
```

in your terminal.

Alternative installation with pip
---------------------------------

| Clone the repository, go to the **dist** folder and run

.. code-block:: console

    pip install cflowpy-0.1-py3-none-any.whl

For development install the optional dependencies for testing, documenting and building with 

.. code-block:: console

    pip install cflowpy-0.1-py3-none-any.whl[dev]

if you further would like to install the ipykernel package use

.. code-block:: console

    pip install cflowpy-0.1-py3-none-any.whl[dev,notebook]

Alternative installation with setup.py
--------------------------------------

| Get the current version of ``cflowpy`` from **dist** and unpack the .tar.gz file.
| Navigate to ``cflowpy-0.1``, for version 0.1, and run

.. code-block:: console
    
    python setup.py install
