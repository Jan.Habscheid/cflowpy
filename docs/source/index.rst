Welcome to cflowpy's documentation!
==============================================

.. admonition:: \ \

   This python package has the purpose of 
   **predicting the flow of an incompressible fluid**.
   It aims to compute the velocity and the pressure field.

| To predict the flow of an incompressible fluid this package 
  package provides a solver for the **incompressible Navier-Stokes equation** 
  in the momentum form.
| The purpose is to solve this with **Chorins Projection method.**
| The following Equation is solved

.. math::

   \frac{\partial u}{\partial t} + (u \cdot \nabla)u = 
   -\frac{1}{\rho} \nabla p + \nu \nabla^2 u + g

| With the additional constraint ∇ • u = 0 for Incompressibility.
| In Terms of variables we talk about the flow velocity for u[:math:`\frac{mm}{s}`],
  the time for t:math:`[s]`, the density for :math:`\rho` [:math:`\frac{kg}{mm^3}`], 
  the pressure for p[:math:`\frac{kg}{mm\cdot s^2}`], 
  the kinematic viscosity for :math:`\nu` [cSt = :math:`\frac{mm^2}{s}`] 
  and body accelerations for g[:math:`\frac{mm}{s^2}`].

| In this library g is set to zero.
| The velocity field :math:`\pmb{u}` has 2 dimensions, the velocity in x-direction
  as well as the velocity in y-direction.
| In this package we use a common definition for this and work with u as the velocity
  in x-direction and v as the velocity in y-direction.

The method
==========
| Chorin's projection method is a rather simple algorithm to solve
  the incompressible Navier-Stokes equation.
| This algorithm is a splitted Algorithm.
| First, the an intermediate velocity is computed
  and only viscous forces are taken into account. 
| Second, the velocity is updated in time by taking pressure 
  forces into account.
| To do so, it is needed to solve a Poisson-Equation.
| Chorin's projection method is an iterativ method. 
| We repeat the two steps above very often to finally reach a stable case
  where the velocity fields and pressure field do change very little.

.. toctree::
   installation
   tutorial
   grid
   physical_problem
   integrator
   system
   :maxdepth: 1
   :caption: cflowpy:
   :hidden:



Indices
=======

* :ref:`genindex`
