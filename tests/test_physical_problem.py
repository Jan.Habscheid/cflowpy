import cflowpy as cf
import numpy as np


def test_Pipe_flow_physical_problem():
    Pipe = cf.Pipe_flow(3)
    u = np.ones((4,4))
    v = np.ones((4,4))
    p = np.ones((4,4))
    u_init, v_init, p_init = Pipe.initialize_system(u, v, p)
    u_init_test = np.array([[0,0,0,0],[3,3,3,3],[3,3,3,3],[0,0,0,0]])
    v_init_test = np.zeros_like(v, float)
    p_init_test = np.zeros_like(p, float)
    assert np.allclose(u_init, u_init_test, rtol=1e-15, atol=1e-15),\
                        'Pipe initialize system for u not working'
    assert np.allclose(v_init, v_init_test, rtol=1e-15, atol=1e-15),\
                        'Pipe initialize system for v not working'
    assert np.allclose(p_init, p_init_test, rtol=1e-15, atol=1e-15),\
                        'Pipe initialize system for p not working'
                
    u[:,-2] = 5
    v[:,-2] = 10
    p[:,1] = 3     
    u_bcs, v_bcs = Pipe.apply_bcs_velocity(u,v)
    p_bcs = Pipe.apply_bcs_pressure(p)
    u_bcs_test = np.array([[0,0,0,0],[3,1,5,5],[3,1,5,5],[0,0,0,0]])
    v_bcs_test = np.array([[0,0,0,0],[0,1,10,10],[0,1,10,10],[0,0,0,0]])
    p_bcs_test = np.array([[3,3,1,1],[3,3,1,1],[3,3,1,1],[3,3,1,1]])
    assert np.allclose(u_bcs, u_bcs_test, rtol=1e-15, atol=1e-15),\
                        'Pipe apply bcs for u not working.'
    assert np.allclose(v_bcs, v_bcs_test, rtol=1e-15, atol=1e-15),\
                        'Pipe apply bcs for v not working.'
    assert np.allclose(p_bcs, p_bcs_test, rtol=1e-15, atol=1e-15),\
                        'Pipe apply bcs for p not working.'
                      


def test_pipe_flow_pressure_driven_physical_problem():
    Pipe_pressure_driven = cf.Pipe_flow_pressure_driven(3, 16, 10)
    u = np.ones((4,4))
    v = np.ones((4,4))
    p = np.ones((4,4))
    u_init_test = np.array([[0,0,0,0],[3,3,3,3],[3,3,3,3],[0,0,0,0]])
    v_init_test = np.zeros_like(v, float)
    p_init_test = np.array([[16.,14.,12.,10.],[16.,14.,12.,10.],[16.,14.,12.,10.],[16.,14.,12.,10.]])
    u_init, v_init, p_init = Pipe_pressure_driven.initialize_system(u, v, p)
    assert np.allclose(u_init, u_init_test, rtol=1e-15, atol=1e-15),\
                        'Pipe initialize system for u not working'
    assert np.allclose(v_init, v_init_test, rtol=1e-15, atol=1e-15),\
                        'Pipe initialize system for v not working'
    assert np.allclose(p_init, p_init_test, rtol=1e-15, atol=1e-15),\
                        'Pipe initialize system for p not working'
                    
    u[:,-2] = 5
    v[:,-2] = 10
    p[:,1] = 3  
    u_bcs, v_bcs = Pipe_pressure_driven.apply_bcs_velocity(u,v)
    p_bcs = Pipe_pressure_driven.apply_bcs_pressure(p)
    u_test = np.array([[0,0,0,0],[3,1,5,5],[3,1,5,5],[0,0,0,0]])
    v_test = np.array([[0,0,0,0],[0,1,10,10],[0,1,10,10],[0,0,0,0]])
    p_bcs_test = p_init_test
    assert np.allclose(u_bcs, u_test, rtol=1e-15, atol=1e-15),\
                        'Pipe apply bcs for u not working.'
    assert np.allclose(v_bcs, v_test, rtol=1e-15, atol=1e-15),\
                        'Pipe apply bcs for v not working.'
    assert np.allclose(p_bcs, p_bcs_test, rtol=1e-15, atol=1e-15),\
                        'Pipe apply bcs for p not working.'



def test_pipe_step_physical_problem():
    Pipe_with_a_step = cf.Pipe_with_a_backward_facing_step(2, 1, 1.5)
    u = np.zeros((4,4), float)
    v = np.zeros_like(u, float)
    p = np.zeros_like(u, float)
    u[:,-2] = 5
    v[:,-2] = 10
    p[:,1] = 3
    u_init, v_init, p_init = Pipe_with_a_step.initialize_system(u, v, p)
    u_init_test = np.array([[0.,0.,0.,0.],[0.,0.,1.5,1.5],\
                            [1.5,1.5,1.5,1.5],[0.,0.,0.,0.]])
    v_init_test = np.zeros_like(v, float)
    p_init_test = np.zeros_like(p, float)
    assert np.allclose(u_init, u_init_test, rtol=1e-15, atol=1e-15),\
                      'Pipe with step initialize system for u not working'
    assert np.allclose(v_init, v_init_test, rtol=1e-15, atol=1e-15),\
                      'Pipe with step initialize system for v not working'
    assert np.allclose(p_init, p_init_test, rtol=1e-15, atol=1e-15),\
                       'Pipe with step initialize system for p not working'
    
    u_bcs, v_bcs = Pipe_with_a_step.apply_bcs_velocity(u, v)
    p_bcs = Pipe_with_a_step.apply_bcs_pressure(p)
    u_bcs_test = np.array([[0.,0.,0.,0.],[0.,0.,5.,5.],\
                           [1.5, 0., 5., 5.,],[0.,0.,0.,0.]])
    v_bcs_test = np.array([[0.,0.,0.,0.],[0.,0.,10.,10.],\
                           [0.,0.,10.,10.],[0.,0.,0.,0.,]])
    p_bcs_test = np.array([[0.,0.,0.,0.],[3.,3.,0.,0.],\
                           [3.,3.,0.,0.],[3.,3.,0.,0.]])
    assert np.allclose(u_bcs, u_bcs_test, rtol=1e-15, atol=1e-15),\
                       'Pipe with step apply bcs for u not working'
    assert np.allclose(v_bcs, v_bcs_test, rtol=1e-15, atol=1e-15),\
                       'Pipe with step apply bcs for v not working'
    assert np.allclose(p_bcs, p_bcs_test, rtol=1e-15, atol=1e-15), \
                       'Pipe with step apply bcs for p not working'



def test_Cavity_flow_physical_problem():
    def test_Cavity_left():
        Cavity_left = cf.Cavity_flow(10, "left")
        u = np.ones((3,3))
        v = np.ones((3,3))
        p = np.ones((4,4))
        u[:,-2] = 5
        v[:,-2] = 10
        p[1,1] = 3
        u_init, v_init, p_init = Cavity_left.initialize_system(u, v, p)
        u_bcs,v_bcs = Cavity_left.apply_bcs_velocity(u, v)
        p_bcs = Cavity_left.apply_bcs_pressure(p)
        assert np.allclose(u_init, u_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(left) initialize system for u not working'
        assert np.allclose(v_init, v_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(left) initialize system for v not working'
        assert np.allclose(p_init, p_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(left) initialize system for p not working'
                      
        u_test_left = np.array([[0,0,0],[0,5,0],[0,0,0]])
        v_test_left = np.array([[10,0,0],[10,10,0],[10,0,0]])
        p_test_left = np.array([[0,3,1,1],[0,3,1,1],[0,1,1,1],[0,1,1,1]])
        assert np.allclose(u_bcs, u_test_left, rtol=1e-15, atol=1e-15),\
                          'Cavity(left) apply bcs for u not working'
        assert np.allclose(v_bcs, v_test_left, rtol=1e-15, atol=1e-15),\
                          'Cavity(left) apply bcs for v not working'
        assert np.allclose(p_bcs, p_test_left, rtol=1e-15, atol=1e-15),\
                           'Cavity(left) apply bcs for p not working'

    def test_Cavity_top():
        Cavity_top = cf.Cavity_flow(10, "top")
        u = np.ones((3,3))
        v = np.ones((3,3))
        p = np.ones((4,4))
        u[:,-2] = 5
        v[:,-2] = 10
        p[1,1] = 3
        u_init, v_init, p_init = Cavity_top.initialize_system(u, v, p)
        u_bcs,v_bcs = Cavity_top.apply_bcs_velocity(u, v)
        p_bcs = Cavity_top.apply_bcs_pressure(p)
        assert np.allclose(u_init, u_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(top) initialize system for u not working'
        assert np.allclose(v_init, v_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(top) initialize system for v not working'
        assert np.allclose(p_init, p_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(top) initialize system for p not working'
                      
        u_test_top = np.array([[0,0,0],[0,5,0],[10,10,10]])
        v_test_top = np.array([[0,0,0],[0,10,0],[0,0,0]])
        p_test_top = np.array([[3,3,1,1],[3,3,1,1],[1,1,1,1],[0,0,0,0]])
        assert np.allclose(u_bcs, u_test_top, rtol=1e-15, atol=1e-15),\
                          'Cavity(top) apply bcs for u not working'
        assert np.allclose(v_bcs, v_test_top, rtol=1e-15, atol=1e-15),\
                           'Cavity(top) apply bcs for v not working'
        assert np.allclose(p_bcs, p_test_top, rtol=1e-15, atol=1e-15),\
                           'Cavity(top) apply bcs for p not working'
    
    def test_Cavity_right():
        Cavity_right = cf.Cavity_flow(10, "right")
        u = np.ones((3,3))
        v = np.ones((3,3))
        p = np.ones((4,4))
        u[:,-2] = 5
        v[:,-2] = 10
        p[1,1] = 3
        u_init, v_init, p_init = Cavity_right.initialize_system(u, v, p)
        u_bcs,v_bcs = Cavity_right.apply_bcs_velocity(u, v)
        p_bcs = Cavity_right.apply_bcs_pressure(p)
        assert np.allclose(u_init, u_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(right) initialize system for u not working'
        assert np.allclose(v_init, v_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(right) initialize system for v not working'
        assert np.allclose(p_init, p_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(right) initialize system for p not working'
                      
        u_test_right = np.array([[0,0,0],[0,5,0],[0,0,0]])
        v_test_right = np.array([[0,0,10],[0,10,10],[0,0,10]])
        p_test_right = np.array([[3,3,1,0],[3,3,1,0],[1,1,1,0],[1,1,1,0]])
        assert np.allclose(u_init, u_test_right, rtol=1e-15, atol=1e-15),\
                         'Cavity(right) apply bcs for u not working'
        assert np.allclose(v_init, v_test_right, rtol=1e-15, atol=1e-15),\
                          'Cavity(right) apply bcs for v not working'
        assert np.allclose(p_init, p_test_right, rtol=1e-15, atol=1e-15),\
                          'Cavity(right) apply bcs for p not working'

    def test_Cavity_bottom():
        Cavity_bottom = cf.Cavity_flow(10, "bottom")
        u = np.ones((3,3))
        v = np.ones((3,3))
        p = np.ones((4,4))
        u[:,-2] = 5
        v[:,-2] = 10
        p[1,1] = 3
        u_init, v_init, p_init = Cavity_bottom.initialize_system(u, v, p)
        u_bcs,v_bcs = Cavity_bottom.apply_bcs_velocity(u, v)
        p_bcs = Cavity_bottom.apply_bcs_pressure(p)
        assert np.allclose(u_init, u_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(bottom) initialize system for u not working'
        assert np.allclose(v_init, v_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(bottom) initialize system for v not working'
        assert np.allclose(p_init, p_bcs, rtol=1e-15, atol=1e-15),\
                      'Cavity(bottom) initialize system for p not working'
                      
        u_test_bottom = np.array([[10,10,10],[0,5,0],[0,0,0]])
        v_test_bottom = np.array([[0,0,0],[0,10,0],[0,0,0]])
        p_test_bottom = np.array([[0,0,0,0],[3,3,1,1],[1,1,1,1],[1,1,1,1]])
        assert np.allclose(u, u_test_bottom, rtol=1e-15, atol=1e-15),\
                          'Cavity(bottom) apply bcs for u not working'
        assert np.allclose(v, v_test_bottom, rtol=1e-15, atol=1e-15),\
                          'Cavity(bottom) apply bcs for v not working'
        assert np.allclose(p, p_test_bottom, rtol=1e-15, atol=1e-15),\
                          'Cavity(bottom) apply bcs for p not working'

    test_Cavity_left()
    test_Cavity_top()
    test_Cavity_right()
    test_Cavity_bottom()
