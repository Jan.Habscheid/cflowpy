import numpy as np

"""
This File is used to implement different types of physical problems.
This makes it possible to pass any kind of physical problem to 
```System```.
Important is the following. 
The wished physical problem, in form of one class of the type PhysicalProblem,
has the desired functions to apply the boundary conditions of the problem.
The function "apply_bcs_velocity" accepts two arguments.
- the velocity in x-direction and the velocity in y-direction
The function "apply_bcs_pressure" accepts one argument.
- the pressure field.
Everybody is welcomed to implement another instance of the class 
PhysicalProblem for a new physical problem.
If the physical problem should start from a specific point,
e.g. with velocities set to one all over the domain,
one also needs to implement a function "initialize_system", with 
u, v and p as arguments.
Otherwise, the initialization is done with the update functions 
for the problem.

Yet, there are a few types of different problems implemented.
For explanation on the specific case have a look in the class of interest.
"""


class PhysicalProblem:
    """
    Base class for all kind of physical problems.

    This class is used as a parent class to different kinds of problems.
    Each inheritant class should have the three Methods of this class.

    Attributes
    ----------
    physical_problem : PhysicalProblem
        Describes the physical problem one wants to work on.
    """

    def __init__(self, 
                 physical_problem):
        """
        Constructor

        Parameters
        ----------
        physical_problem : type[PhysicalProblem]
            Subclass of PhysicalProblem
        """
        if not isinstance(physical_problem, PhysicalProblem):
            raise TypeError("physical_problem has to be a subclass "
                            "of PhysicalProblem")

        self.physical_problem = physical_problem

    def initialize_system(self, 
                          u: np.ndarray, 
                          v: np.ndarray, 
                          p: np.ndarray):
        """
        Creates the initial state of the problem.

        By default, the boundary conditions are applied.
        However, some of the subclasses can have a specific
        starting point for the physical problem.

        Parameters
        ----------
        u : np.ndarray
            Velocity in x-direction before applying the boundary conditions
        v : np.ndarray
            Velocity in y-direction before applying the boundary conditions
        p : np.ndarray
            Pressure field before applying the boundary conditions

        Raises
        ------
        TypeError
            If one of the inputs is no numpy ndarray

        Returns
        -------
        np.ndarray, np.ndarray, np.ndarray
            Velocity in x and y-direction as well as the pressure after
            applying the boundary conditions.
        """
        if not isinstance(u, np.ndarray):
            raise TypeError("u has to be a numpy ndarray")
        if not isinstance(v, np.ndarray):
            raise TypeError("v has to be a numpy ndarray")
        if not isinstance(p, np.ndarray):
            raise TypeError("p has to be a numpy ndarray")

        u, v = self.apply_bcs_velocity(u, v)
        p = self.apply_bcs_pressure(p)
        
        return u, v, p

    def apply_bcs_velocity(self, 
                           u: np.ndarray, 
                           v: np.ndarray):
        """
        Applies the boundary conditions for the velocities.

        Parameters
        ----------
        u : np.ndarray
            Velocity in x-direction
        v : np.ndarray
            Velocity in y-direction

        Raises
        ------
        TypeError
            If one of the inputs is no numpy ndarray

        Returns
        -------
        np.ndarray, np.ndarray
            Velocities with applied boundary conditions
        """
        if not isinstance(u, np.ndarray):
            raise TypeError("u has to be a numpy ndarray")
        if not isinstance(v, np.ndarray):
            raise TypeError("v has to be a numpy ndarray")
        
        return self.physical_problem.apply_bcs_velocity(u, v)

    def apply_bcs_pressure(self,
                           p: np.ndarray):
        """
        Applies boundary conditions for the pressure field.

        Parameters
        ----------
        p : np.ndarray
            Pressure field

        Raises
        ------
        TypeError
            If p is no numpy ndarray

        Returns
        -------
        np.ndarray
            Pressure field with applied boundary conditions
        """
        if not isinstance(p, np.ndarray):
            raise TypeError("p has to be a numpy ndarray")
        
        return self.physical_problem.apply_bcs_pressure(p)



class Pipe_flow(PhysicalProblem):
    """
    This class is used to simulate a pipe.

    At the left edge of the domain there is used 
    a Dirichlet boundary condition.
    (velocity going into the pipe)
    We assume the pipe to be long enough to get the full velocity profile.
    To do so, there are used Neumann boundary conditions on the right edge 
    of the domain.
    To assure the no-slip condition there are used Dirichlet boundary 
    conditions at the top and bottom edge of the domain to ensure the 
    velocity is zero there.

    Attributes
    ----------
    u_left : float
        Velocity at the left side of the domain in x-direction
    """

    def __init__(self, 
                 u_left: float):
        """
        Constructor

        Parameters
        ----------
        u_left : float
            Velocity in x-direction going from the left side into the domain.

        Raises
        ------
        TypeError
            If u_left is no number
        """
        if not isinstance(u_left, (float, int)):
            raise TypeError("Velocity in x-direction on the "
                            "left side of the domain has to be a number")
        self.u_left = u_left

    def initialize_system(self, 
                          u: np.ndarray, 
                          v: np.ndarray, 
                          p: np.ndarray):
        """
        Initialize system 

        Initializes velocity in x-direction with inflow velocity and 
        velocity in y-direction and pressure with zero.

        Parameters
        ----------
        u : np.ndarray
            velocity field in x-direction
        v : np.ndarray
            velocity field in y-direction
        p : np.ndarray
            pressure field

        Raises
        ------
        TypeError
            If one of the inputs is no numpy ndarray

        Returns
        -------
        np.ndarray, np.ndarray, np.ndarray
            Velocity in x and y-direction as well as the pressure after
            applying the boundary conditions.
        """
        if not isinstance(u, np.ndarray):
            raise TypeError("u has to be a numpy ndarray")
        if not isinstance(v, np.ndarray):
            raise TypeError("v has to be a numpy ndarray")
        if not isinstance(p, np.ndarray):
            raise TypeError("p has to be a numpy ndarray")
        
        u = np.full_like(u, self.u_left)
        v = np.zeros_like(v, float)
        p = np.zeros_like(p, float)
        
        # no slip-condition
        u[-1, :] = 0
        u[0, :] = 0
        
        return u, v, p

    def apply_bcs_velocity(self, 
                           u: np.ndarray,
                           v: np.ndarray):
        """
        Applies the boundary conditions for the velocity field.

        Applies Dirichlet boundary conditions on the left side of the domain.

        Parameters
        ----------
        u : np.ndarray
            velocity field in x-direction
        v : np.ndarray
            velocity field in y-direction

        Raises
        ------
        TypeError
            If one of the inputs is no numpy ndarray

        Returns
        -------
        np.ndarray, np.ndarray
            Velocity fields after applying the boundary conditions
        """
        if not isinstance(u, np.ndarray):
            raise TypeError("u has to be a numpy ndarray")
        if not isinstance(v, np.ndarray):
            raise TypeError("v has to be a numpy ndarray")

        # Enforce boundary conditions for velocity in x-direction
        # left
        u[:, 0] = self.u_left
        # right
        u[:, -1] = u[:, -2]
        # bottom
        u[0, :] = 0
        # top
        u[-1, :] = 0

        # Enforce boundary conditions for velocity in y-direction
        # left
        v[:, 0] = 0
        # right
        v[:, -1] = v[:, -2]
        # bottom
        v[0, :] = 0
        # top
        v[-1, :] = 0

        return u, v

    def apply_bcs_pressure(self,
                           p:np.ndarray):
        """
        Applies the boundary conditions for the pressure field.

        Applies Neumann boundary conditions on each side of the domain.

        Parameters
        ----------
        p : np.ndarray
            pressure field

        Raises
        ------
        TypeError
            If the pressure field is no numpy ndarray

        Returns
        -------
        np.ndarray
            Pressure field after applying the boundary conditions
        """
        if not isinstance(p, np.ndarray):
            raise TypeError("p has to be a numpy ndarray")
        
        # left
        p[:, 0] = p[:, 1]
        # right
        p[:, -1] = p[:, -2]
        # bottom
        p[0, :] = p[1, :]
        # top
        p[-1, :] = p[-2, :]

        return p



class Pipe_flow_pressure_driven(Pipe_flow):
    """
    This class is used to simulate a pressure driven pipe

    In this class, Dirichlet boundary conditions are used to describe the
    pressure on the left and on the right side of the domain.
    The pressure in the domain itself is linearly interpolated from 
    the prescribed pressure on the boundaries.
    Besides this, the boundary conditions for the velocity are still the same
    as for the parent Class "Pipe_flow".
    This class extends "Pipe_flow" and "initialize_system" and 
    "apply_bcs_velocity" are used from Pipe_flow, for information 
    regarding this functions have a look in the parent class.

    Attributes
    ----------
    u_left : float
        Velocity at the left edge of the domain in x-direction
    p_left : float
        Pressure at the left edge of the domain
    p_right : float
        Pressure at the right edge of the domain
    """
    def __init__(self, u_left:float, p_left:float, p_right:float):
        """
        Constructor

        Parameters
        ----------
        u_left : float
            Velocity at the left edge of the domain in x-direction
        p_left : float
            Pressure at the left edge of the domain
        p_right : float
            Pressure at the right edge of the domain
            
        Raises
        ------
        TypeError
            If u_left is no number
        TypeError
            If either p_left or p_right is no number
        """
        if not isinstance(u_left, (float, int)):
            raise TypeError("Velocity in x-direction on the "
                            "left side of the domain has to be a number")
        if not isinstance(p_left, (float, int)):
            raise TypeError("Pressure at left side has to be a number")
        if not isinstance(p_right, (float, int)):
            raise TypeError("Pressure at right side has to be a number")
        
        self.u_left = u_left
        self.p_left = p_left
        self.p_right = p_right
        
    def initialize_system(self, u:np.ndarray, v:np.ndarray, p:np.ndarray):
        """
        Initialize system 

        Initializes velocity in x-direction with inflow velocity, 
        velocity in y-direction with zero and pressure with 
        the prescribed pressure gradient.

        Parameters
        ----------
        u : np.ndarray
            velocity field in x-direction
        v : np.ndarray
            velocity field in y-direction
        p : np.ndarray
            pressure field

        Raises
        ------
        TypeError
            If one of the inputs is no numpy ndarray

        Returns
        -------
        np.ndarray, np.ndarray, np.ndarray
            Velocity in x and y-direction as well as the pressure after
            applying the boundary conditions.
        """
        if not isinstance(u, np.ndarray):
            raise TypeError("u has to be a numpy ndarray")
        if not isinstance(v, np.ndarray):
            raise TypeError("v has to be a numpy ndarray")
        if not isinstance(p, np.ndarray):
            raise TypeError("p has to be a numpy ndarray")
        u, v, p = super().initialize_system(u, v, p)
        p = self.apply_bcs_pressure(p)
        
        return u, v, p

    def apply_bcs_velocity(self, u:np.ndarray, v:np.ndarray):
        """
        Applies boundary conditions for velocity using parent class.
        """
        return super().apply_bcs_velocity(u, v)
    
    def apply_bcs_pressure(self, p:np.ndarray):
        """
        Applies boundary conditions for the pressure

        Pressure driven Pipe
        The pressure on the left and right side of the domain are prescriben
        and the pressure inside the domain is interpolated from this.
        Therefore it can be useful to set "n_iteration_pressure_poisson"
        to zero, when solving the incompressible Navier-Stokes equation 
        with "System".

        Parameters
        ----------
        p : np.ndarray
            pressure field

        Raises
        ------
        TypeError
            If the pressure field is no numpy ndarray

        Returns
        -------
        np.ndarray
            Pressure field after applying the boundary conditions
        """
        if not isinstance(p, np.ndarray):
            raise TypeError("p has to be a numpy ndarray")
        
        p_domain = np.linspace(self.p_left, self.p_right, len(p[0,:]))
        for i in range(len(p[:,0])):
            p[i,:] = p_domain
        
        return p



class Pipe_with_a_backward_facing_step(Pipe_flow):
    """
    This class is used to simulate a pipe with a backward facing step.

    At the left edge of the domain there is used a 
    Dirichlet boundary condition, (velocity going into the pipe) 
    but only above the step.
    In the step itself, the pressure and all velocities are set to zero.
    All remaining boundary conditions stay the same as 
    for the "normal" pipe flow
    This class extends "Pipe_flow".

    Attributes
    ----------
    x_step_End : int
        Index of the right edge of the step in the grid
    u_left : float
        Velocity, in x-direction, at the left side 
        of the domain above the step
    y_step_End : int
        Index of the top edge of the step in the grid
    """

    def __init__(self, 
                 x_step_End: int, 
                 y_step_End: int, 
                 u_left: float, ):
        """
        Constructor

        Parameters
        ----------
        x_step_End : int
            Index for the end of the step in x-direction.
            Be careful: This is just the index for the end of the step
            in the vector, not the physically located end of the step.
        y_step_End : int
            Index for the end of the step in y-direction.
            Be careful: This is just the index for the end of the step
            in the vector, not the physically located end of the step.
        u_left : float
            Velocity in x-direction going from the left side into the domain.

        Raises
        ------
        TypeError
            If either the x_step_End or y_step_End is no integer
        ValueError
            If either x_step_end or y_step_end is smaller or equal 0
        TypeError
            If u_left is no number
        """
        if not isinstance(x_step_End, int):
            raise TypeError("End of step in x-direction has to be an integer")
        if not isinstance(y_step_End, int):
            raise TypeError("End of step in y-direction has to be an integer")
        if x_step_End <= 0 or y_step_End <= 0:
            raise ValueError("End of the step has to be positive \
                              The given end is the index of the end "
                              "of the step in the grid, \
                              not the position itself.")
        if not isinstance(u_left, (float, int)):
            raise TypeError("Velocity in x-direction on the "
                            "left side of the domain has to be a number")

        self.u_left = u_left
        self.x_step_End = x_step_End
        self.y_step_End = y_step_End

    def initialize_system(self, 
                          u:np.ndarray, 
                          v:np.ndarray, 
                          p:np.ndarray):
        """
        Initializes the start of the system.

        In this case we want to start with the velocity in x-direction
        beeing everywhere, except in the step, the value for u_left.
        The velocity in y-direction and the pressure are not touched and
        therefore are zero.

        Parameters
        ----------
        u : np.ndarray
            Velocity in x-direction
        v : np.ndarray
            Velocity in y-direction
        p : np.ndarray
            Pressure field

        Raises
        ------
        TypeError
            If one of the inputs is no numpy ndarray
        Returns
        -------
        np.ndarray, np.ndarray, np.ndarray
            Initialized system
        """
        
        u, v, p = super().initialize_system(u, v, p)
        u, v = self.apply_bcs_velocity(u, v)

        return u, v, p

    def apply_bcs_velocity(self, 
                           u:np.ndarray, 
                           v:np.ndarray):
        """
        Applies the boundary conditions for the velocity field.

        Applies Dirichlet boundary conditoins on the left side of the domain
        of the domain and above the step.
        Everywhere als Neumann boundary conditions are applied
        and inside the step all velocities are set to zero
        Parameters
        ----------
        u : np.ndarray
            velocity field in x-direction
        v : np.ndarray
            velocity field in y-direction

        Raises
        ------
        TypeError
            If one of the inputs is no numpy ndarray

        Returns
        -------
        np.ndarray, np.ndarray
            Velocity fields after applying the boundary conditions
        """
        if not isinstance(u, np.ndarray):
            raise TypeError("u has to be a numpy ndarray")
        if not isinstance(v, np.ndarray):
            raise TypeError("v has to be a numpy ndarray")
        # Enforce boundary conditions for velocity in x-direction
        # left
        u[:, 0] = self.u_left
        # right
        u[:, -1] = u[:, -2]
        # bottom
        u[0, :] = 0
        # top
        u[-1, :] = 0

        # Enforce boundary conditions for velocity in y-direction
        # left
        v[:, 0] = 0
        # right
        v[:, -1] = v[:, -2]
        # bottom
        v[0, :] = 0
        # top
        v[-1, :] = 0

        # velocity is zero inside the step
        u[: self.y_step_End, : self.x_step_End] = 0
        v[: self.y_step_End, : self.x_step_End] = 0

        # no slip condition -> velocity at boundary of the step is zero
        # top edge of the step
        u[self.y_step_End, : self.x_step_End] = 0
        v[self.y_step_End, : self.x_step_End] = 0
        # right edge of the step
        u[: self.y_step_End, self.x_step_End] = 0
        v[: self.y_step_End, self.x_step_End] = 0

        return u, v
    
    

    def apply_bcs_pressure(self, 
                           p:np.ndarray):
        """
        Applies the boundary conditions for the pressure field.

        Applies Neumann boundary conditions on each side of the domain
        In the step itself, the pressure is set to zero.

        Parameters
        ----------
        p : np.ndarray
            pressure field

        Raises
        ------
        TypeError
            If the pressure field is no numpy ndarray

        Returns
        -------
        np.ndarray
            Pressure field after applying the boundary conditions
        """
        if not isinstance(p, np.ndarray):
            raise TypeError("p has to be a numpy ndarray")

        # left
        p[:, 0] = p[:, 1]
        # right
        p[:, -1] = p[:, -2]
        # bottom
        p[0, :] = p[1, :]
        # top
        p[-1, :] = p[-2, :]

        # no pressure inside the step
        p[: self.y_step_End, : self.x_step_End] = 0
        # top edge of the step
        p[self.y_step_End, : self.x_step_End] = \
            p[self.y_step_End + 1, : self.x_step_End]
        # right edge of the step
        p[: self.y_step_End, self.x_step_End] = \
            p[: self.y_step_End, self.x_step_End + 1]
        
        return p


class Cavity_flow(PhysicalProblem):
    """
    This class is used to simulate a lid-driven cavity flow.

    It is dealt with a cavity consisting of three rigid walls 
    with no-slip conditions and a lid moving with a tangential unit velocity.
    For the pressure there are used Dirichlet boundary conditions on the side 
    of the lid and Neumann boundary conditions on all other sides.

    Parameters
    ----------
    velocity : float
        Velocity of the lid
    side : string
        Side of the domain on which the lid is moving
    """

    def __init__(self, 
                 velocity: float, 
                 side: str):
        """
        Constructor

        Parameters
        ----------
        velocity : float
            Velocity of the lid
        side : str
            Side of the domain on which the lid is moving
            Can be one of the following: left, bottom, right or top
        """
        if not isinstance(velocity, (float, int)):
            raise TypeError("Velocity of the lid has to be a number")
        if side not in ["left", "bottom", "right", "top"]:
            raise ValueError("Side not known.",
                             "Side has to be one of the following:\
                              left, bottom, right, top")
        self.velocity = velocity
        self.side = side

    def initialize_system(self, 
                          u:np.ndarray, 
                          v:np.ndarray, 
                          p:np.ndarray):
        """
        Initialize system by applying the boundary conditions.

        No typechek is needed, since the super function checks for the types.

        Parameters
        ----------
        u : np.ndarray
            velocity field in x-direction
        v : np.ndarray
            velocity field in y-direction
        p : np.ndarray
            pressure field

        Returns
        -------
        np.ndarray, np.ndarray, np.ndarray
            Initial state of the velocities and the pressure.
        """
        return super().initialize_system(u, v, p)

    def apply_bcs_velocity(self, u, v):
        """
        Applies the boundary conditions for the velocity field.

        Applies Dirichlet boundary conditions on each side of the domain,

        Parameters
        ----------
        u : np.ndarray
            velocity field in x-direction
        v : np.ndarray
            velocity field in y-direction

        Raises
        ------
        TypeError
            If one of the inputs is no numpy ndarray

        Returns
        -------
        np.ndarray, np.ndarray
            Velocity fields after applying the boundary conditions
        """

        if not isinstance(u, np.ndarray):
            raise TypeError("u has to be a numpy ndarray")
        if not isinstance(v, np.ndarray):
            raise TypeError("v has to be a numpy ndarray")

        # Enforce boundary conditions for velocity in x-direction
        # left
        u[:, 0] = 0
        # right
        u[:, -1] = 0
        # bottom
        u[0, :] = 0
        # top
        u[-1, :] = 0

        # Enforce boundary conditions for velocity in y-direction
        # left
        v[:, 0] = 0
        # right
        v[:, -1] = 0
        # bottom
        v[0, :] = 0
        # top
        v[-1, :] = 0

        # updates velocity on the side of the lid
        if self.side == "left":
            v[:, 0] = self.velocity
        elif self.side == "right":
            v[:, -1] = self.velocity
        elif self.side == "bottom":
            u[0, :] = self.velocity
        elif self.side == "top":
            u[-1, :] = self.velocity

        return u, v

    def apply_bcs_pressure(self, 
                           p:np.ndarray):
        """
        Applies the boundary conditions for the pressure field

        Applies Dirichlet boundary conditions on the side of the lid
        and Neumann boundary conditions on all other sides.

        Parameters
        ----------
        p : np.ndarray
            pressure field

        Raises
        ------
        TypeError
            If the pressure field is no numpy ndarray

        Returns
        -------
        np.ndarray
            Pressure field after applying the boundary conditions
        """

        if not isinstance(p, np.ndarray):
            raise TypeError("p has to be a numpy ndarray")

        # Enforce boundary conditions for pressure
        # left
        p[:, 0] = p[:, 1]
        # right
        p[:, -1] = p[:, -2]
        # bottom
        p[0, :] = p[1, :]
        # top
        p[-1, :] = p[-2, :]

        if self.side == "left":
            p[:, 0] = 0
        elif self.side == "right":
            p[:, -1] = 0
        elif self.side == "bottom":
            p[0, :] = 0
        elif self.side == "top":
            p[-1, :] = 0

        return p
