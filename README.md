# cflowpy


[![Pipeline Status](https://git.rwth-aachen.de/Jan.Habscheid/cflowpy/badges/main/pipeline.svg)](https://git.rwth-aachen.de/Jan.Habscheid/cflowpy/pipelines) 
[![Documentation](https://img.shields.io/badge/docs-latest-brightgreen)](https://janhab.pages.rwth-aachen.de/cflowpy/) 
[![GitLab Version](https://img.shields.io/badge/version-1.0-blue.svg)](https://git.rwth-aachen.de/jan.habscheid/cflowpy/-/tags)
[![License](https://img.shields.io/badge/license-MIT-blue)](https://git.rwth-aachen.de/Jan.Habscheid/cflowpy/-/blob/main/LICENSE?ref_type=heads)

A python package for simulating fluid flows with chorins projection method.

cflowpy is a package to solve the incompressible Navier-Stokes Equation with Chorin's projection method.

## Installation

```console
pip install git+https://git.rwth-aachen.de/Jan.Habscheid/cflowpy
```

### Alternative installation

It is also possible to clone the repository, move to **dist** and run

```console
pip install cflowpy-0.1-py3-none-any.whl
```

if you would like to use the package in a jupyter notebook you can directly install the ipykernel package together with cflowpy by running

```console
pip install cflowpy-0.1-py3-none-any.whl[notebook]
```

and for development run

```console
pip install cflowpy-0.1-py3-none-any.whl[dev]
```

Another possibility is to unpack the .tar.gz file and from ``cflowpy-0.1`` run

```console
python setup.py install
```

## Usage

Open python and **import cflowpy** with

```python
import cflowpy as cf
```

Now you are ready to solve the incompressible Navier-Stokes Equation to *predict*
the behavior of an *incompressible fluid flow*.

### A simple example

If you would like to simulate a *lid-driven cavity* flow you can do this with:

```python
physical_problem = cf.Cavity_flow(1, "top")
system = cf.System(0, 1, 30, 0, 1, 30, 0.1, 1, physical_problem)
u, v, p = system.solve(0.001, 1)
```

You can now visualize the results with

```python
fig = system.contourfigure(u, v, p, velocity="quiver", cmap="coolwarm", title="Lid-driven cavity flow")
```

if we would like to save the simulation results in another file we can do this by running the simulation with the ``save_data`` command

```python
u, v, p = system.solve(0.001, 1, save_data="data")
```

These information are stored in a newly generated ``data.npz`` file and can be accessed with numpys load function.

## Tests

If you have installed all necessary dependencies for tests you can run

```console
python -m pytest
```

in the package directory.
There are a few unit-tests for each functionality.
Furthermore, one test run for the Lid-driven cavity flow is compared against stored results from a sample run.

## Documentation

For a detailed tutorial, more reference on the package and different physical problems to simulate have a look at the **documentation.**
The documentation ca be found in ``docs/build/html``.
Just open the ``index.html`` and click through the documentation for further information.
