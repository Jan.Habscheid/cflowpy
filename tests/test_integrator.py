import cflowpy as cf
import numpy as np


def test_integrator():
    integrator = cf.Integrator()
    test_methods = ["explicit_Euler", "Runge_Kutta_4"]
    assert(integrator.get_methods() == test_methods)
    
    def test_explicit_Euler():
        f = np.array([[1,2,3],[4,5,6],[7,8,9]])
        dT = 0.1
        rhs = np.full_like(f, -3, dtype=float)
        f_expl_Euler = integrator.explicit_Euler(f, dT, rhs)
        f_expl_Euler_test = np.array([[0.7,1.7,2.7],[3.7,4.7,5.7],[6.7,7.7,8.7]])
        assert np.allclose(f_expl_Euler, f_expl_Euler_test, rtol=1e-7, atol=1e-7), 'Explicit Euler not working'
    
    def test_Runge_Kutta_4():
        f = np.array([[1,2,3],[4,5,6],[7,8,9]])
        dT = 0.1
        rhs = np.full_like(f, -3, dtype=float)
        f_RK4 = integrator.Runge_Kutta_4(f, dT, rhs)
        f_RK4_test = np.array([[0.63442917,1.55434583,2.4742625],[3.39417917,4.31409583,5.2340125],[6.15392917,7.07384583,7.9937625]])
        assert np.allclose(f_RK4, f_RK4_test, rtol=1e-7, atol=1e-7), 'Runge Kutta 4 not working'
        
    test_explicit_Euler()
    test_Runge_Kutta_4()
