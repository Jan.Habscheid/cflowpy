import cflowpy as cf
import numpy as np


def test_grid():
    
    def test_geometry():
        grid = cf.Grid(0, 2, 10, 0, 1, 10)
        assert(grid.get_dx() == 0.2)
        assert(grid.get_dy() == 0.1)
        assert np.allclose(grid.get_u(), np.zeros((11,11)),\
                           rtol=1e-15, atol=1e-15)
        assert np.allclose(grid.get_v(), np.zeros((11,11)),\
                           rtol=1e-15, atol=1e-15)
        assert np.allclose(grid.get_p(), np.zeros((11,11)),\
                           rtol=1e-15, atol=1e-15)

        x_test, y_test = np.meshgrid(np.linspace(0,2,11), np.linspace(0,1,11))
        assert np.allclose(grid.get_x(), x_test, rtol=1e-15, atol=1e-15)
        assert np.allclose(grid.get_y(), y_test, rtol=1e-15, atol=1e-15)
    
    def test_fd():
        grid1 = cf.Grid(0, 4, 4, 0, 4, 4)
        grid2 = cf.Grid(0, 8, 4, 0, 8, 4)
        grid1.u = np.array([[1,2,3,4],[2,4,5,6],[3,7,15,11],[20,23,22,21]])
        grid2.u = np.array([[1,2,3,4],[2,4,5,6],[3,7,15,11],[20,23,22,21]])
        fd_y = np.array([[0,0,0,0],[1,2.5,6,3.5],[9,9.5,8.5,7.5],[0,0,0,0]])
        fd_x = np.array([[0,1,1,0],[0,1.5,1,0],[0,6,2,0],[0,1,-1,0]])
        assert np.allclose(grid1.central_fd(grid1.get_u(), 1, 1),\
                           fd_y, rtol=1e-15, atol=1e-15),\
                           'central finite-difference in\
                            y-direction not working'
        assert np.allclose(grid2.central_fd(grid2.get_u(), 1, 1),\
                           fd_y*1/2, rtol=1e-15, atol=1e-15),\
                           'central finite-difference in y-direction\
                            for d=2 not working'
        assert np.allclose(grid1.central_fd(grid1.get_u(), 0, 1),\
                           fd_x, rtol=1e-15, atol=1e-15),\
                           'central finite-difference in x-direction\
                            not working'
        assert np.allclose(grid2.central_fd(grid2.get_u(), 0, 1),\
                           fd_x*1/2, rtol=1e-15, atol=1e-15),\
                           'central finite-difference in x-direction\
                            for d=2 not working'
        f_laplace_1 = np.array([[0,0,0,0],[0,0,8,0],[0,17,-15,0],[0,0,0,0]])
        f_laplace_2 = np.array([[0,0,0,0],[0,0,2,0],\
                                [0,4.25,-3.75,0],[0,0,0,0]])
        assert np.allclose(grid1.laplace(grid1.get_u()), f_laplace_1,\
                           rtol=1e-15, atol=1e-15),\
                           'Laplace-operator1 not working'
        assert np.allclose(grid2.laplace(grid2.get_u()), f_laplace_2,\
                           rtol=1e-15, atol=1e-15),\
                           'Laplace-operator2 not working'
        
    def test_jacobi():
        f = np.array([[1,2,3],[4,5,6],[7,8,9]])
        rhs = np.ones_like(f)
        grid = cf.Grid(0, 4, 4, 0, 4, 4)
        J1 = grid.jacobi(f, rhs, 1)
        J2 = grid.jacobi(f, rhs, 2)
        J1_test = np.array([[0,0,0],[0,4.75,0],[0,0,0]])
        J2_test = grid.jacobi(J1_test, rhs, 1)
        assert np.allclose(J1, J1_test, rtol=1e-4, atol=1e-4),\
                           'Jacobi integrator not working'
        assert np.allclose(J2, J2_test, rtol=1e-4, atol=1e-4),\
                           'Jacobi integrator not working'
        physical_problem = cf.Pipe_flow(3)
        J_bcs = grid.jacobi(f, rhs, 1, \
                            physical_problem.apply_bcs_pressure)
        J_bcs_test = np.full_like(f, J1_test[1,1], dtype=float)
        assert np.allclose(J_bcs, J_bcs_test, rtol=1e-4, atol=1e-4), \
                           'Boundary update in Jacobi integrator not working'
        
    test_geometry()
    test_fd()
    test_jacobi()
    