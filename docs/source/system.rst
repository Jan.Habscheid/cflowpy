Solving the incompressible Navier-Stokes Equation
=================================================

| This is the heart of cflowpy.
| ``System`` describes the system on which we want to simulate the
  incompressible Navier Stokes Equation.

  .. warning::

    | Take care of how to choose the discretization, when 
      initializing the ``System``.
    | A to coarse discretization might lead to unstable results and a 
      to fine discretization might lead to overflow errors.
    | For some suggestions on implemented physical problems see the 
      documentation of those in :any:`physical_problem`

.. autoclass:: src.System
    :noindex:

Constructor
-----------

| The Constructor specifies the physical domain, the viscosity 
  and density of the fluid. 
| Furthermore, it describes the physical state of the 
  problem, which specifies the initial state and the boundary conditions. 

.. automethod:: src.System.__init__

Solve
-----

.. note::
   | This is the function which gives us the possibility to predict the
     velocity and pressure field of an incompressible fluid, since this 
     function solve the incompressible Navier-Stokes equation.

.. automethod:: src.System.solve

Numerical stability
-------------------

| The cfl-condition is a widely used condition to check for numerical stability.
| For this kind of simulation we want to fullfill this condition.

    - The cfl-number is: :math:`\frac{u \cdot dt}{dx}`

.. automethod:: src.System.check_stability

| If the cfl-condition is not fullfilled there are two possibilities.
| First, if one wants to continue with the computations it would be good to
  adapt some values to ensure that the cfl-condition is fullfilled again.
| Since u and dx are fixed the only way to ensure stability is to change dt.
| Second, one could stop the simulation and work with the results till the time
  the cfl-condition broke. 

| If it is wished to adapt the time step size to fullfill the cfl-condition
  this is done with

.. automethod:: src.System.update_dt

Stepping forward in time
------------------------
| The incompressible Navier-Stokes Equation is a time dependant PDE.
| This means, a method to step forward in time is needed.
| Therefore, :std:ref:`Integrator` is used.
| It is possible to choose between the implemented 
  methods in :std:ref:`Integrator`.
| Another possibility is to use an :std:ref:`Adaptive step size method`, which adapts
  the step size in each time step to reach a higher accuracy.
| However, this can lead to a higher compute time since the time steps 
  can get arbitrary small.

.. automethod:: src.System.update_time

Termination criterion
---------------------

| The matter of interest could be a case where neither the velocities
  nor the pressure changes over time. 
| ``check_residual`` enables the possibility to stop the computation 
  when the change in two consecutive time steps is smaller 
  then a ``residual``.

.. note::
    The computations will still be stopped if the final time is achieved.
    To achieve a case with no changes over time it is still important 
    to pass an end time which is high enough to ensure this.

.. automethod:: src.System.check_residual
    
Saving the results
------------------

The results can be saved in a **.npz** file for reproducibility.

.. automethod:: src.System.save_data

| To read the .npz file numpys load function can be used.
| Every value can be accessed in a dictionary-like object.
| For example it is possible to read the data and access 
  the discretization in x-direction with the following:

.. code-block:: python

    >>> import numpy as np
    >>> npzfile = np.load(outfile)
    >>> npzfile["x"]

Visualization
-------------

| It is possible to the visualize the results of the simulation.
| Two functions are available to do so.
| ``contourfigure`` visualizes a specific time point of the simulation, 
  most likely the final state.
| ``create_gif`` creates a gif to visualize the change of 
  velocity and pressure over time.

.. automethod:: src.System.contourfigure

.. automethod:: src.System.create_gif