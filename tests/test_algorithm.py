import cflowpy as cf
import numpy as np



def test_Algorithm():    
    physical_problem_cavity = cf.Cavity_flow(1, "top")
    sample = np.load("tests/sample.npz")
    x_start = sample["x"][0]
    x_end = sample["x"][-1]
    nCellsX = len(sample["x"])-1
    y_start = sample["y"][0]
    y_end = sample["y"][-1]
    nCellsY = len(sample["y"])-1
    viscosity = sample["viscosity"]
    density = sample["density"]
    integration_method = sample["integration_method"]
    dt = sample["dt"]
    t_end = sample["end_time"]
    u_sample = sample["velocity_x_direction"]
    v_sample = sample["velocity_y_direction"]
    p_sample = sample["pressure"]
    system_test = cf.System(x_start, x_end, nCellsX,
                            y_start, y_end, nCellsY,
                            viscosity, density,
                            physical_problem_cavity)
    u_test, v_test, p_test = system_test.solve(float(dt), float(t_end),
                                               str(integration_method))
    
    assert np.allclose(u_sample[-1], u_test, rtol=1e-15, atol=1e-15),\
                       "Sample run for u_velocity not correct"
    assert np.allclose(v_sample[-1], v_test, rtol=1e-15, atol=1e-15),\
                       "Sample run for v_velocity not correct"
    assert np.allclose(p_sample[-1], p_test, rtol=1e-15, atol=1e-15),\
                       "Sample run for pressure not correct"
    
    