Tutorial
========

| In this tutorial we want to learn how to use this package.
| This is explained in the case of a lid-driven cavity flow.

| For reference regarding the installation have a look at the :std:ref:`Installation`

Loading the package
-------------------
| First, one needs to import the package.
| This can be done with

.. code-block:: python
   
   >>> import cflowpy as cf

Initializing the physical problem
------------------------------------
| Next, we need to specify the physical problem in order 
  of boundary conditions.
| In this example we want to simulate a Cavity flow, so we use the
  the class ``Cavity_flow`` for specifying this.
| We want to have a flow on top of the domain from left to right 
  with a velocity of one.
| This we can initialize with:

.. code-block:: python
   
   >>> physical_problem_cavity = cf.Cavity_flow(1, "top")

Initializing the system
-----------------------
| In this example we use a Grid on the domain (0,2) in x-direction 
  and (0,2) in y-direction.
| In this case we want to simulate water with a kinematic viscosity of
  0.1[mm^2/s] and 1[kg/L] for the density.

.. code-block:: python
   
   >>> system = cf.System(0, 2, 30, 
                          0, 2, 30, 
                          0.1, 1, 
                          physical_problem_cavity)

Running the simulation
----------------------
| Now we start the simulation with a time step length of 0.001 
  and stop after 3 seconds

.. code-block:: python
   
   >>> u, v, p = system.solve(0.01, 5)

Visualizing the results
-----------------------
| We can use the Visualizing routine from cflowpy with 
  the maplotlib colormap "coolwarm"

.. code-block:: python

   >>>  fig = system.contourfigure(u, v, p, 
                                   cmap="coolwarm", 
                                   title="Lid-driven cavity flow")

.. image:: images/Cavity_quiver.png
   :width: 600

| To get a contourfigure where the pressure is visualized with 
  a colormap and the velocity visualized as arrows.
| If one wants to get streamlines instead of the velocity field, 
  it is possible to use the optional argument ``velocity`` as "streamline"

.. code-block:: python

   >>> fig = system.contourfigure(u, v, p, 
                                  velocity="streamline", 
                                  cmap="coolwarm", 
                                  title="Lid-driven cavity flow")

.. image:: images/Cavity_streamline.png
   :width: 600

| Now we would like to create a gif to visualize the velocities 
  and the pressure over time.
| Therefore, we can use the ```create_gif``` routine and save the gif 
  in the "example.gif" file.
| We only want to visualize every 10th time step and want each frame 
  to last for 5ms.
| However, to do so we need to get the history of the velocities and 
  the pressure over time and rerun the simulation with the ```history``` argument set to true.

.. code-block:: python
   
   >>> u, v, p = system_cavity.solve(0.001, 3, history=True)
   >>> fig = system.create_gif("example.gif", 
                               u, v, p, 
                               velocity="streamline", 
                               cmap="coolwarm",
                               skip_time=10, duration=5,
                               title="Lid-driven cavity flow")

.. figure:: images/Cavity.gif
   :alt: StreamPlayer
   :align: center
   :width: 600