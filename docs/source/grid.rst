Grid 
====

| In the following, one can find the purpose of the class ``Grid`` 
  and it's attributes.

.. note::

   | The class ``Grid`` is used in ``System``.
   | However, it is not needed to create an instance of ``Grid``
     by the user itself, this is done in ``System`` itself.    

.. autoclass:: src.Grid
    :noindex:

Constructor
-----------    

The Constructor of ``Grid`` works with the following:

.. automethod:: src.Grid.__init__

Get routines
------------

``Grid`` has the following routines to get attributes of the class itself.
    
.. automethod:: src.Grid.get_x 
.. automethod:: src.Grid.get_y
.. automethod:: src.Grid.get_dx
.. automethod:: src.Grid.get_dy
.. automethod:: src.Grid.get_u
.. automethod:: src.Grid.get_v
.. automethod:: src.Grid.get_p

Calculation routines
--------------------

| Furthermore ``Grid`` has several routines to do calculations
  with finite differences.
| It is possible to compute the central finite differences of
  order 1 and 2 on coordinates of the grid.
| Additionally, it is possible to get the Laplace Operator.
| A Poisson Equation can be solved with the Jacobi method
  on this grid, too.

.. automethod:: src.Grid.central_fd
.. automethod:: src.Grid.central_fd_deg1
.. automethod:: src.Grid.central_fd_deg2
.. automethod:: src.Grid.laplace
.. automethod:: src.Grid.jacobi