Physical Problem 
================

| The class ``PhysicalProblem`` is used as a base class
  to implement **any kind of physical problem.**

.. autoclass:: src.PhysicalProblem
    :noindex:

Constructor
-----------

.. automethod:: src.PhysicalProblem.__init__


System initialisation
---------------------

| The initial state of each problem is specified in ``initialize_system``.
| If this is not further specialised in the child class the velocities and 
  the pressure are initiales to zero at all points and then the boundary 
  conditions are applied.

.. automethod:: src.PhysicalProblem.initialize_system

Boundary Conditions
-------------------

| The boundary conditions are enforced after every iteration step in 
  Chorin's projection method.
| It is possible to implement any kind of boundary conditions.

.. automethod:: src.PhysicalProblem.apply_bcs_velocity
.. automethod:: src.PhysicalProblem.apply_bcs_pressure


Subclasses
----------

| Yet, there are a few different Subclasses implemented, each defining 
  another physical problem.

.. toctree::
   PhysicalProblems/pipe_flow
   PhysicalProblems/pipe_with_a_backward_facing_step
   PhysicalProblems/cavity_flow
   :maxdepth: 2
   :caption: Contents:
   :glob: